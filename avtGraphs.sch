<Qucs Schematic 0.0.20>
<Properties>
  <View=-60,-210,1440,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=avtGraphs.dat>
  <DataDisplay=avtGraphs.dpl>
  <OpenDisplay=1>
  <Script=avtGraphs.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
</Components>
<Wires>
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Arrow 400 330 0 -270 20 8 #000000 0 1 0>
  <Arrow 30 70 0 270 20 8 #000000 0 1 0>
  <Arrow 30 70 270 0 20 8 #000000 0 1 0>
  <Text 20 350 12 #000000 0 "a (m/s^2)">
  <Text 290 40 12 #000000 0 "t(s)">
  <Text 400 40 12 #000000 0 "v (m/s)">
  <Line 30 290 250 0 #5555ff 2 1>
  <Text -40 280 12 #000000 0 "-9.81">
  <Line 20 290 20 0 #000000 0 1>
  <Arrow 400 110 270 0 20 8 #000000 0 1 0>
  <Text 680 100 12 #000000 0 "t (s)">
  <Line 400 80 240 240 #5555ff 2 1>
  <Text 350 310 12 #000000 0 "-29.7\n">
  <Text 360 80 12 #000000 0 "2.00">
  <Line 410 80 -20 0 #000000 0 1>
  <Line 410 320 -20 0 #000000 0 1>
  <Arrow 400 270 0 80 20 8 #000000 0 1 0>
  <Line 640 100 0 20 #000000 0 1>
  <Text 620 80 12 #000000 0 "3.23">
  <Arrow 790 330 0 -270 20 8 #000000 0 1 0>
  <Arrow 790 330 270 0 20 8 #000000 0 1 0>
  <Text 770 40 12 #000000 0 "x (m)">
  <Text 1070 320 12 #000000 0 "t (s)">
  <Line 780 100 20 0 #000000 0 1>
  <Text 740 90 12 #000000 0 "45.0">
  <Line 1020 320 0 20 #000000 0 1>
  <Text 1010 350 12 #000000 0 "3.23">
  <Line 260 80 0 -20 #000000 0 1>
  <Text 240 40 12 #000000 0 "3.23">
  <EArc 550 100 470 420 5675 1459 #5555ff 2 1>
</Paintings>
