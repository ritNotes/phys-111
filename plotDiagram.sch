<Qucs Schematic 0.0.20>
<Properties>
  <View=0,-60,1360,989,1,0,120>
  <Grid=10,10,1>
  <DataSet=plotDiagram.dat>
  <DataDisplay=plotDiagram.dpl>
  <OpenDisplay=1>
  <Script=plotDiagram.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
</Components>
<Wires>
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Rectangle 520 190 90 50 #000000 0 1 #c0c0c0 1 0>
  <Text 530 200 12 #000000 0 "Camera">
  <Arrow 570 190 0 -40 20 8 #000000 0 1 0>
  <Arrow 570 240 0 60 20 12 #000000 0 1 0>
  <Line 520 210 -90 0 #000000 0 1>
  <Arrow 440 360 0 -150 20 8 #000000 0 1 0>
  <Arrow 440 400 0 130 20 8 #000000 0 1 0>
  <Text 410 370 12 #000000 0 "45.0m">
  <Arrow 550 240 0 60 20 12 #000000 0 1 0>
  <Text 520 310 12 #000000 0 "-9.81m/s^2">
  <Line 130 530 910 0 #000000 0 1>
  <Rectangle 520 480 90 50 #000000 0 1 #c0c0c0 1 0>
  <Text 530 490 12 #000000 0 "Camera">
  <Arrow 540 530 0 60 20 12 #000000 0 1 0>
  <Arrow 520 530 0 60 20 12 #000000 0 1 0>
  <Text 490 600 12 #000000 0 "-9.81m/s^2">
  <Text 610 584 12 #000000 0 "-29.7m/s">
  <Text 550 120 12 #000000 0 "2.00m/s">
  <Arrow 600 530 0 120 20 8 #000000 0 1 0>
</Paintings>
