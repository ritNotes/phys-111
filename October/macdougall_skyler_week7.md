# Week 7 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/9/2020

# Conservative and Non-Conservative Forces

$$
W=Fd\cos\theta\\
\theta=\ang d-\ang F
$$



1. Write the work-kinetic energy theorem in words and as a formula.

    The total work done equals the change in kinetic energy.
2. A book of mass $m$ moves along a horizontal table where a kinetic frictional force of constant magnitude of $5.0N$ acts on the book. Point $B$ is located $2.0m$ north and $1.5m$ east of point $A$. The book is motionless at the start and at the end. 
    1. Determine the work done by the force of friction along the path $A\rightarrow C\rightarrow B$. Make a sketch of $\overrightarrow {f_k}$, $\overrightarrow d$, and $\theta$ for each calculation of work.
        $$
        A\rightarrow C\\
        \overrightarrow d=2.0m\ang90^\circ;\ \overrightarrow{f_k}=5.0N\ang-90^\circ;\\
        W=Fd\cos\theta\\
        W_1=5.0N*2.0m*\cos(90^\circ-(-90^\circ))\\
        W_1=10.0Nm*-1\\
        W_1=-10.0J\\\rule{70pt}{0.4pt}\\
        C\rightarrow B\\
        \overrightarrow d=1.5m\ang0^\circ;\ \overrightarrow{f_k}=5.0N\ang180^\circ;\\
        W_2=5.0N*1.5m*\cos(0-180^\circ)\\
        W=-7.5J\\\rule{70pt}{0.4pt}\\
        W_{total}=\sum W\\
        W_{total}=(-10.0J)+(-7.5J)\\
        W_{total}=-17.5J
        $$
        
    2. Determine the work done by the force of friction along the path $A\rightarrow B$. Make a sketch of $\overrightarrow {f_k}$, $\overrightarrow d$, and $\theta$ for the calculation of work.
        $$
        \overrightarrow d=(\sqrt{(2.0m)^2+(1.5m)^2})\ang(\tan^{-1}(\frac{1.5m}{2.0m}))^\circ;\ F=5.0N\ang(\tan^{-1}(\frac{20.m}{1.5m}))^\circ\\
        \overrightarrow d=2.5m\ang53.13010235^\circ;\ F=5.0N\ang-126.8698976^\circ\\
        W=5.0N*2.5m*\cos(180^\circ)\\
        W=-12.5J
        $$
        
    3. Are your answers the same or different for the two paths?
        The answers are different for the two paths.
    
    4. Using the work-kinetic energy theorem, find the total work done on the book in going from point $A$ to point $B$. This is the work done by ALL  forces acting on the book.
        $$
        W=K_f-K_i\\
        K=\frac12mv^2\\
        v_i=0=v_f\\
        \therefore\\
        W=0
        $$
        
    5. Is the work done by the friction force equal to the total work? Why or why not?
        The friction force is not equal to the total work. There is another force that caused the movement of the book, which is counteracted by the friction, equating to zero work.
3. Now consider a ball of mass $m$ that moves in the vertical plane. Points $A,\ B$, and $C$ are the same as before. You move from $A$ to $B$ along two different paths. The ball has a mass of $4.0kg$ and is motionless at the start and end (points $A$ and $B$ respectively.)
    1. Determine the work done by the weight force along the path $A\rightarrow C\rightarrow B$. Make a sketch of $\overrightarrow {f_k}$, $\overrightarrow d$, and $\theta$ for each calculation of work.
        $$
        A\rightarrow C\\
        \overrightarrow d=2.0m\ang90^\circ;\ \overrightarrow{f_k}=39.24N\ang-90^\circ;\\
        W=Fd\cos\theta\\
        W_1=39.24N*2.0m*\cos(90^\circ-(-90^\circ))\\
        W_1=78.48J*-1\\
        W_1=-78.48J\\\rule{70pt}{0.4pt}\\
        C\rightarrow B\\
        \overrightarrow d=1.5m\ang0^\circ;\ \overrightarrow{f_k}=39.24N\ang-90^\circ;\\
        W_2=39.24*1.5m*\cos(0-90^\circ)\\
        W=0J\\\rule{70pt}{0.4pt}\\
        W_{total}=\sum W\\
        W_{total}=(-78.48J)+(0J)\\
        W_{total}=-78.48J
        $$
        
    2. Determine the work done by the weight force along the path $A\rightarrow B$. Make a sketch of $\overrightarrow {f_k}$, $\overrightarrow d$, and $\theta$ for the calculation of work.
        $$
        \overrightarrow d=(\sqrt{(2.0m)^2+(1.5m)^2})\ang(\tan^{-1}(\frac{1.5m}{2.0m}))^\circ;\ F=39.24N\ang-90^\circ\\
        \overrightarrow d=2.5m\ang53.13010235^\circ;\ F=39.24N\ang-90^\circ\\
        W=39.24N*2.5m*\cos(53.13010235^\circ-(-90^\circ))\\
        W=-78.48J
        $$
        
    3. Are your answers the same or different for the two paths?
        The answer is the same for both paths.
    
    4. Using the work-kinetic energy theorem, find the total work done on the book in going from point $A$ to point $B$. This is the work done by ALL  forces acting on the book.
        $$
        W=K_f-K_i\\
        K=\frac12mv^2\\
        v_i=0=v_f\\
        \therefore\\
        W=0
        $$
        
    5. Is the work done by the weight force equal to the total work? Why or why not?
        The friction force is not equal to the total work. There is another force that caused the movement of the book, which is counteracted by the friction, equating to zero work.

# Energy Conservation

1. A slingshot fires a pebble from the top of a building at a speed of $14.0m/s$. The bulding is $31.0m$ tall. Assume an ideal system. Find the speed with which the pebble strikes the ground when the pebble is fired:

    1. Horizontally.
    2. Straight up.
    3. Straight down.
    4. At an angle of $\theta=27.5^\circ$ above the horizontal.

    All of these will be the same, at:
    $$
    W=\Delta K+\Delta U\\
    K=\frac12mv^2;\ U=mgy\\
    W=(\frac12m(v_f^2-v_i^2))+(mg\Delta y)\\
    W=0\\
    \therefore\\
    \frac12m(v_f^2-v_i^2)=mg\Delta y\\
    \frac12(v_f^2-v_i^2)=g\Delta y\\
    v_f^2-v_i^2=2g\Delta y\\
    v_f^2=2g\Delta y+v_i^2\\
    v_f=\sqrt{2g\Delta y+v_i^2}\\
    v_f=\sqrt{2(9.81m/s^2)(31.0m)+(14.0m/s)^2}\\
    v_f=28.35677289m/s\\
    \overline{\underline{|v_f=28.4m/s|}}
    $$

2. A cart slides to the right along a smooth surface and passes position 1 ($x_i$) with a speed of $15.0m/s=v_i$. Ignore all frictional effects. The vertical height at each position is labeled.

    1. What is the speed of the cart as it passes position 2 (+8.00m vertical)?
        Smooth surface = Conservative
        $$
        K_1+U_1=K_2+U_2\\
        K=\frac12mv^2;\ U=mgy\\
        \frac12mv_1^2+mgy_1=\frac12mv_2^2+mgy_2\\
        \frac12mv_1^2+mg(0)=\frac12mv_22^2+mgy_2\\
        \frac12\cancel{m}v_1^2+\cancel{mg(0)}^0=\frac12\cancel{m}v_2^2+\cancel{m}gy_2\\
        \frac12v_2^2=\frac12v_2^2-gy_2\\
        v_2^2=v_1^2-2gy_2\\
        v_2=\sqrt{v_1^2-2gy_2}\\
        v_2=\sqrt{(15.0m/s)^2-2(9.81m/s^2)(8.00m)}\\
        v_2=8.248636251m/s\\
        \overline{\underline{|v_2=8.25m/s|}}
        $$

    2. Will the cart reach position 4? If so, what is the speed as it passes position 4?
        Potential energy at most must be equal to kinetic energy, because of conservation of energy. Therefore:
        $$
        \frac12mv^2=mgy\\
        \frac12v^2=gy\\
        \frac{v^2}{2g}=y\\
        y=11.46788991m\\
        11.46788991m\cancel{\ge}15.0m\\
        \therefore
        $$
        The cart will not reach position 4.

3. In an emergency, the passengers on an airplane slide down a curved escape ramp to safety. The ramp has a radius of curvature $R=10.0m$ and the passengers exit the plane $2.50m$ above the ground. 

    1. What is the change in gravitational potential energy of a $72.0kg$ passenger due to sliding down the ramp?
        $$
        \Delta U=mg\Delta y\\
        \Delta U=(72.0kg)(9.81m/s^2)(2.50m)\\
        \Delta U=1.7658kJ\\
        \overline{\underline{|\Delta U=1.77kJ|}}
        $$
    
2. What is the passenger's speed at the bottom of the slide, assuming they start at rest at the top of the slide?
        Assuming an energy-conservative system:
    $$
        \Delta U=\Delta K\\
        K=\frac12mv^2\\
        \Delta U=\frac12mv^2\\
        \sqrt\frac{2\Delta U}{m}=v\\
        \sqrt\frac{2(1.77kJ)}{72.0kg}=v\\
        v=7.003 570 518 m/s\\
        \overline{\underline{|v=7.00m/s|}}
    $$
    
    3. If the passenger reaches the bottom of the slide with a speed of $3.75m/s$, how much work was done on the passenger by non-conservative forces? What forces are responsible for the non-conservative work?
    $$
        K=\frac12mv^2\\
        K=\frac12(72.0kg)(3.75m/s)^2\\
        K=506.25J\\
        K_{ideal}-K_{real}\\
        1.77kJ-506.25J\\
        1.25955kJ
    $$
    $1.30kJ$ of work is done by non-conservative forces. The force most likely to have done this work is friction.
    
4. A vertical spring with a spring constant of $k$ is mounted on the floor. From directly above the spring, which is unstrained, a block of mass $m$ is dropped from rest. It collides with and sticks to the spring, which compresses a maximum amount $x_{max}$ in bringing the block to a momentary halt. Assume an ideal system.

    1. From what height $H$ above the compressed spring was the block dropped? [^1]
        $$
        U_g=U_{spring}\\
        U_g=mgy;\ U_{spring}=\frac12kx^2;\ y=H\\
        mgH=\frac12kx^2\\
        H=\frac{kx^2}{2mg}
        $$

    2. Calculate $H$ given:
        $$
        x_{max}=2.50cm\\
        m=0.300kg\\
        k=450N/m
        $$
        

    $$
    H=\frac{kx^2}{2mg}\\
    H=\frac{(450N/m)(2.50cm)^2}{2(0.300kg)(9.81m/s)}\\
    H=47.782 874 62 mm\\
    \overline{\underline{|H=4.78cm|}}
    $$

    

5. A spring has a constant $k$. The spring is near the base of a ramp inclined at an angle $\theta$ above the horizontal. A block of mass $m$ is pressed against the spring, compressing it a distance $x_i$ from its equilibrium position. The block is then released. The horizontal and inclined surfaces are smooth. The block is not attached to the spring.

    1. What is the maximum distance it travels up the inclined plane? [^1]
        $$
        K=\frac12mv^2\\
        U_g=mgy\\
        mgy=\frac12mv^2\\
        y=\frac{\frac12v^2}{g}\\
        \sin(\theta)=\frac OH\\
        H=\frac{O}{\sin(\theta)}\\
        d=\frac{\frac{\frac12v^2}{g}}{\sin\theta}\\
        F=ma=-kx\\
        a=\frac{-kx}m\\
        v_f^2=v_i^2+2ax\\
        v_i=0\\
        v_f^2=2\frac{-kx}mx\\
        d=\frac{\frac{\frac12(2\frac{-kx^2}m)}{g}}{\sin\theta}\\
        d=\frac{\frac{\frac{-kx^2}m}{g}}{\sin\theta}\\
        d=\frac{{\frac{-kx^2}{mg}}}{\sin\theta}\\
        d=\frac{-kx^2}{mg\sin\theta}
        $$
    
2. Calculate the numerical value for the maximum distance it travels up the inclined plane given the following:
        $$
        k=40.0N/m\\
        \theta=30.0^\circ\\
        m=0.500kg\\
        x_i=20.0cm
        $$
    
    $$
        d=\frac{-kx^2}{mg\sin\theta}\\
        d=\frac{-(40.0N/m)(20.0cm)^2}{(0.500kg)(-9.81m/s^2)\sin(30.0^\circ)}\\
        d=32.619776mm\\
        d=32.6mm
    $$
    
    
    
3. When the block slides down the inclined plane and collides with the spring , what is the maximum compression of the spring?
        Due to conservation of energy, the spring will be compressed exactly as much as the initial compression ($20.0cm$).
    
6. A piece of wood of mass $m$ slides on the surface shown below. All parts of the surface are frictionless, except a $30.0m$ long rough flat segment in the bottom, where the coefficient of kinetic friction with the wood is $\mu_k$. The wood starts at rest a vertical distance $y_i$ above the bottom.

    1. How far to the right of the left edge of the rough segment will the wood eventually come to rest? [^1]
    $$
        U=K\\
        mg\Delta y=\frac12mv^2\\
        v^2={2g\Delta y}\\
        v_f^2=v_i^2+2a{\Delta x}\\
        0=2g\Delta y+2a{\Delta x}\\
        \Delta y=0-y_i
    F=ma;\ F=\mu_kmg;\ ma=\mu_kmg;\ a=\mu_kg\\
        0=2gy+2(\mu_kg){\Delta x}\\
        \frac{-2g\Delta y}{2\mu_kg}=x\\
        \frac{-\Delta y}{\mu_k}=x\\
        \frac{y_i}{\mu_k}=x\\
    $$
    
    
    2. Calculate a numerical value for the distance to the right of the left edge of the rough segment that the wood eventually comes to rest given the following:
        $$
        m=2.00kg\\
        \mu_k=0.200\\
        y_i=4.00m
        $$
    
        $$
        \frac{y_i}{\mu_k}=x\\
        \frac{4.00m}{0.200}=x\\
        x=20.0m
        $$
        
        
        
    3. How much work is done by friction by the time the wood stops?
        $$
        U=K=W_{friction}\\
        \therefore\\
        W_{friction}=mg\Delta y\\
        W_{friction}=(2.00kg)(9.81m/s^2)(-4.00m)\\
        W_{friction}=78.48J\\
        \overline{\underline{|W_{friction}=78.5J|}}
        $$
        

# Energy Graphs

1. In the graph below, a cart is released from rest at point A and slides without friction along the vertical track. The cart is always in contact with the track.
![image-20201008154437925](macdougall_skyler_week7.assets/image-20201008154437925.png)
   
    1. What is the kinetic energy of the cart at point A?
        If the cart is at rest, then the kinetic energy of the cart is zero.
    2. How is the total mechanical energy at point A related to the gravitational potential energy at point A?
        They are the same.
    3. Draw a dashed horizontal line representing the total mechanical energy $E$ on the graph.
    4. Label with the letter $B$ the point at which the potential energy has its lowest value.
    5. At point B, show by using arrows how the potential energy and the kinetic energy add to give the constant mechanical energy $E$. Make sure you identify each arrow appropriately. 
    6. What is the value of the kinetic energy at a turning point?
        The kinetic energy is not changing at a turning point, and either growing more positive or more negative. This is when the velocity is changing from positive to negative, or vice versa.
    7. Identify the turning points on the graph with a letter T.
    8. identify the point at which the cart will reach its maximum height with the letter M.
2. A cart slides without friction along the vertical track. The total energy $E$ of the cart is indicated in the diagram. The cart does not leave the track.
    ![image-20201008154448127](macdougall_skyler_week7.assets/image-20201008154448127.png)
    
    1. Is the object at rest at point A?
        No.
    2. Identify the point at which the object will reach it maximum height with the letter M. How does this point compare to the maximum location in Question 1?
        The point at which the object will reach its maximum height is higher for question 2 than in question 1. This is because the object has a higher total energy in question 1, leading to more potential energy at its peak.
3. A $1.60kg$ object in a conservative system moves along the x-axis, where the potential energy as a function of position is shown in the graph. The value for $U\ @\ x=0,2$ are shown as $9.35J$ and $4.15J$. 
    ![image-20201008154455947](macdougall_skyler_week7.assets/image-20201008154455947.png)
    
    1. If the object's speed at $x=0$ is $2.30m/s$, what is its speed at $2.00m$?
        $$
        U_1+K_1=U_2+K_2\\
        U_1= 9.35J;\ U_2=4.15J;\\ v_1=2.30m/s;\ K=\frac12mv^2\\
        U_1+\frac12v_1^2=U_2+\frac12v_2^2\\
        \frac{2(U_1-U_2)}m+v_1^2=v_2^2\\
        \sqrt{\frac{2(U_1-U_2)}m+v_1^2}=v_2\\
        \sqrt{\frac{2(9.35J-4.15J)}{1.60kg}+(2.30m/s)^2}=v_2\\
        v_2=3.433 656 943 m/s\\
        \overline{\underline{|v_2=3.43m/s|}}
        $$
    
    2. What is the value for the mechanical energy in the system?
        $$
        Mechanical\ Energy=U+K\\
        U=9.35J;\ K=\frac12mv^2;\ m=1.60kg;\ v=2.30m/s\\
        U+\frac12mv^2\\
        9.35J+\frac12(1.60kg)(2.30m/s)^2\\
        Mechanical\ Energy=13.582J\\
        \overline{\underline{|Mechanical\ Energy=13.6J|}}
        $$
        
    3. What is the value of the potential energy at the turning points?
    
        At turning points, the potential energy is the same as the mechanical energy, at $13.6J$.
    
        

