# Week 10 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/30/2020

# Work, Rotational Energy and Energy Conservation

1. A $12.0g$ CD with a radius of $6.00cm$ rotates with an angular speed of $34.0rad/s$. Treat the CD as a uniform disk.

    1. What is its kinetic energy?
        $$
        K=\frac12I\omega^2;\ I=mr^2\\
        K=\frac12mr^2\omega^2\\
        K=\frac12(12.0g)(6.00cm)^2(34.0rad/s)^2\\
        K=0.0249696J\\
        \overline{\underline{|K=25.0mJ|}}
        $$

    2. What angular speed must the CD have if its kinetic energy is doubled?
        $$
        \omega=\sqrt{\frac{2K}{mr^2}}\\
        \omega=\sqrt{\frac{2(50.0mJ)}{(12.0g)(6.00cm)^2}}\\
        \omega=48.083261rad/s\\
        \overline{\underline{|\omega=48.1rad/s|}}
        $$

    3. What average power is required to bring it to rest from its original speed in $2.50s$?
        $$
        P=\tau\omega;\ \tau=I\alpha;\ \alpha=\frac{\omega}{t};\ I=mr^2\\
        P=\frac{mr^2\omega^2}{t}\\
        P=\frac{(12.0g)(6.00cm)^2(34.0rad/s)^2}{2.50s}\\
        P=0.01997568W\\
        \overline{\underline{|P=20.0mW|}}
        $$

2. When a baseball pitcher throws a curve ball, the ball is given a fairly rapid spin. If a $0.150kg$ baseball with a radius of $3.70cm$ is thrown with a linear center-of-mass speed of $48.0m/s$ and an angular speed of $42.0rad/s$ around its center-of-mass, how much of its total kinetic energy is translational kinetic energy of its center-of-mass and how much is rotational kinetic energy around its center-of-mass? Assume the ball is a uniform solid sphere.
    $$
    K_{rot}=\frac12I\omega^2;\ K_{trans}=\frac12mv^2;\ I=mr^2\\
    K_{rot}=\frac12mr^2\omega^2|K_{trans}=\frac12mv^2\\
    K_{rot}=\frac12(0.150kg)(3.70cm)^2(42.0rad/s)^2|K_{trans}=\frac12(0.150kg)(48.0m/s)^2\\
    K_{rot}=0.1811187J|K_{trans}=172.8J\\
    K_{total}=172.98112J\\
    K_{rot_\%}=\frac{K_{rot}}{K_{total}}=\frac{0.1811187J}{172.98112J}=0.10470432\%\\
    K_{trans_\%}=\frac{K_{trans}}{K_{total}}=\frac{172.8J}{172.98112J}=99.895296\%\\
    \overline{|K_{rot_\%}=0.10\%|}\\
    \underline{|K_{trans_\%}=99.90\%|}
    $$

3. A wheel of radius $R$ is free to rotate about a frictionless axle. The moment of inertia of the wheel with respect to its axis of rotation is $I$. A light rope is wrapped around the fixed wheel. The other end of the rope is attached to a box of mass $m$. The box is released at rest. The rope does not stretch or slip over the surface of the wheel. $I$ will be given as an intertial value, do not break it down.

    1. Use conservation of energy to derive an expression of the **speed** $v$, of the box after it has descended a vertical distance $h$. [^1]
        $$
        U=mgh;\ \Delta K=\frac12I\omega^2;\ \omega=\frac{v_t}{r}\\
        h_i=0;\ v_{t_i}=0\\
        U_i+K_i=U_f+K_f\\
        mgh_i+\frac{Iv_i^2}{2r^2}+\frac12mv^2=mgh_f+\frac{Iv_f^2}{2r^2}\\
        \cancel{mgh_i}^0+\cancel{\frac{Iv_i^2}{2r^2}}^0+\cancel{\frac12mv^2}^2=mgh_f+\frac{Iv_f^2}{2r^2}+\frac12mv^2\\
        -mgh_f=\frac{Iv_f^2}{2r^2}+\frac12mv^2\\
        v^2(\frac{I+mr^2}{2r^2})=-mgh_f\\
        v=\sqrt{\frac{-2mgh_fr^2}{I+mr^2}}
        $$
        
2. Determine the speed of the box given the following:
        $$
        h=1.50m\\
        R=0.200m\\
        I=0.400kg\times m^2\\
        m=6.00kg
        $$
    
    $$
        v_f=\sqrt{\frac{-2mgh_fr^2}{I+mr^2}}\\
        v_f=\sqrt{\frac{-2(6.00kg)(9.81m/s^2)(-1.50m)(0.200m)^2}{0.400kg\times m^2+(6.00kg)(0.200m)^2}}\\
        v_f=3.3220852m/s\\
        \overline{\underline{|v_f=3.32m/s|}}
        $$
    
3. Using your result for part 3.1, write the speed of the box after it has descended a distance $h$ assuming the pulley is "*light*". Does this result make sense? What is the numerical value of the speed of the box in the limiting case of a "*light*" pulley?
        $$
        v_f=\sqrt{\frac{-2mgh_fr^2}{I+mr^2}};\ I=0\\
        v_f=\sqrt{\frac{-2mgh_fr^2}{\cancel{I}^0+mr^2}}\\
        v_f=\sqrt{\frac{-2\cancel{m}gh_f\cancel{r^2}}{\cancel{mr^2}}}\\
        v_f=\sqrt{-2gh_f}\\
        v_f=\sqrt{-2(9.81m/s^2)(1.50m)}\\
        v_f=5.4249424m/s\\
        \overline{\underline{|v_f=5.42m/s|}}
        $$

# Torque

1. A $1.40m$ long bar on a horizontal surface is free to rotate about an axis perpendicular to the page and passing through its center. A force $\overrightarrow F$ of magnitude $35.0N$ is applied to the left end of the bar at different angles as shown in the overhead views below. Determine the torque on the bar produced by each of the forces shown below. 

    1.  ![image-20201027160607756](macdougall_skyler_week10.assets/image-20201027160607756.png)
        $$
        \tau=-rF\sin\theta\\
        \sin\theta=\sin(0^\circ)=0\\
        \tau=0
        $$

    2. ![image-20201027160635126](macdougall_skyler_week10.assets/image-20201027160635126.png)
        $$
        \tau=-rF\sin\theta\\
        \tau=-(1.40m)(35.0N)\sin(90^\circ)\\
        \overline{\underline{|\tau=-49.0J|}}
        $$
        
3.  ![image-20201027160703612](macdougall_skyler_week10.assets/image-20201027160703612.png)
        $$
        \tau=-rF\sin\theta\\
        \overrightarrow{F_{1_y}}=-\overrightarrow{F_{2_y}}\\
        \tau=0
        $$
    
4.  ![image-20201027160731415](macdougall_skyler_week10.assets/image-20201027160731415.png)
        $$
        \tau=-rF\sin\theta\\
        \tau=-(1.40m)(35.0N)\sin(30.0^\circ)\\
        \tau=-24.5J
        $$
    
2. A rod is being used as a lever as shown in the diagram. The fulcrum is $1.2m$ from the load and $2.4m$ from the applied force. The rod makes a $25.0^\circ$ angle with respect to the horizontal. The applied force $\overrightarrow{F_A}$ has a magnitude of $128N$ and is directly down. What is the torque due to the applied force relative to the fulcrum?
    $$
    \tau=-rF\sin\theta\\
    \tau=-(2.4m)(128N)\sin(90^\circ+25^\circ)\\
    \tau=-278.41775J\\
    \overline{\underline{|\tau=278.J,\ clockwise|}}
    $$
    
3. A house painter stands $3.00m$ above the ground on a $5.00m$ long ladder that leans against the wall at a point $4.70m$ above the ground. The painter weighs $680N$ and stands vertically on the ladder. The ladder weighs $120N$. The ladder makes an angle $\theta$ with the horizontal ground. Choose an axis of rotation perpendicular to the page and passing down through the bottom of the ladder. 

    1. Create an extended free body diagram for the ladder using the diagram below. 

    2. Find the torque due to the ladder.
        $r=2.5m$ because that is the distance between the center of mass of the ladder and the point of rotation.
        $$
        \tau=-rF\sin\theta;\ \theta=\sin^{-1}(\frac{h_1}{l_l})_+90^\circ\\
        \tau=-rF\sin(\sin^{-1}(\frac{h_1}{l_l})+90^\circ)\\
        \tau=-(2.50m)(120N)(\sin(\sin^{-1}(\frac{4.70m}{5.00m})+90^\circ))\\
        \tau=-102.3523327J\\
        \overline{\underline{|\tau=102.J,\ clockwise|}}
        $$

    3. Find the torque due to the painter.
        $$
        \tau=-rF\sin\theta;\ r=\frac{3.00m}{\sin(\theta-90^\circ)}\\
        \tau=-(\frac{3.00m}{\sin(\sin^{-1}(\frac{4.70m}{5.00m}))})*680N*\sin(\sin^{-1}(\frac{4.70m}{5.00m})+90^\circ)\\
        \tau=-740.4211298J\\
        \overline{\underline{|\tau=740.J,\ clockwise|}}
        $$
        

# Equilibrium

1. Your job is to ensure the safety of the house painter shown in the diagram below. You quickly realize that preventing the ladder from slipping requires a friction force one the bottom of the ladder. The house painter stands $3.0m$ above the ground on the $5.0m$ long ladder that leans at rest against a smooth vertical wall at a point $4.7m$ above the ground. The painter weighs $680N$ and the uniform ladder weighs $120N$. Assume no friction between the house and the upper end of the ladder.

    1. What is the direction of the static frictional force exerted on the ladder by the ground?
        Towards the wall.

    2. Draw a free body diagram.

    3. Calculate the magnitude of the static frictional force exerted on the ladder by the ground. 
        $$
        \sum\overrightarrow F=0\\
        \therefore\\
        \overrightarrow {n_w}=\overrightarrow{F_{static}}\\
        \overrightarrow {n_g}=\overrightarrow{W_L}+\overrightarrow{W_P}\\
        \tau_{n_w}=\tau_{w_L}+\tau_{w_P}\\
        \tau=rF\sin\theta\\
        \overrightarrow{F_{static}}=\frac{\tau_{n_w}}{r\sin\theta}\\
        \overrightarrow{F_{static}}=\frac{\tau_{w_P}+\tau_{w_L}}{r\sin\theta}\\
        r=l_l;\ \theta=\cos^{-1}(\frac{h_l}{l_l})\\
        \overrightarrow{F_{static}}=\frac{740.J+102.J}{5.00m(\sin(\sin^{-1}(\frac{4.70m}{5.00m})))}\\
        \overrightarrow {F_{static}}=179.3135027N\\
        \overline{\underline{|\overrightarrow {F_{static}}=179.N|}}
        $$

    4. What is the minimum coefficient of static friction required to keep the ladder from slipping?
        $$
        F_{static}=\mu N\\
        N=n_g=\overrightarrow{W_L}+\overrightarrow{W_P}\\
        179.N=\mu (680.N+120.N)\\
        \mu=0.2241418783\\
        \overline{\underline{|\mu=0.224|}}
        $$

2. A $40.0kg,\ 5.00m$ long uniform horizontal beam is supported by, but not attached to, the two vertical posts. The vertical posts are separated by $3.00m$. A $25.0kg$ girl starts walking along the beam to the right.

    1. Determine an algebraic expression for the normal force $N_1$ exerted on the beam by the left vertical post when the girl is a distance $x$ to the right of the vertical post. The result should be $N_1$ written as a function of $x$.
        $$
        F=0;\ \tau=0\\
        N_1+N_2=W_b+W_g\\
        \tau_{N_1}+\tau_{N_2}+\tau_{W_b}+\tau_{W_g}=0\\
        \tau=rF\sin\theta;\ r_{\tau_{N_1}}=0;\ \tau_{N_1}=0\\
        \tau_{N_2}=\tau_{W_b}+\tau_{W_g}\\
        \tau_{W_b}=1.5W_b;\ \tau_{W_g}=(3+X)W_g;\ \tau_{N_2}=3N_2\\
        N_2=\frac{1.5W_b+(3+X)W_g}{3}\\
        N_1+\frac{1.5W_b+(3+X)W_g}{3}=W_b+W_g\\
        N_1+\frac12W_b+(1+\frac13X)W_g=W_b+W_g\\
        N_1=\frac12W_b+(1-(1+\frac13X))W_g\\
        N_1=\frac12W_b-\frac13XW_g
        $$

    2. What is the maximum distance to the right of the right vertical post $x_{max}$ she can walk before the beam begins to tip?
        $$
        
        $$

3. A man is attempting to raise a uniform $34.0kg$ flagpole that is hinged at the base by pulling on a rope to the top of the pole, as shown. Let the length of the flagpole be $L$.

    1. Draw a free body diagram.

    2. With what force does the person have to pull on the rope to hold the pole motionless in the position shown?
        $$
        \tau=rF\sin\theta;\ \sum\tau=0\\
        \tau_{pole}=\tau_{man}\\
        \frac L2mg\sin(120^\circ)=LF\sin(170^\circ)\\
        F=\frac{mg\sin(120^\circ)}{2\sin(170^\circ)}\\
        $$
        
3. What are the horizontal and vertical components of the force exerted on the bottom of the pole by the hinge?
        $$
        asdf
        $$
        