# Week 9 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/23/2020

# Rotational Kinematics

1. A computer disk starts from rest at time $0$, and accelerates uniformly for $0.500s$ to an angular speed of $2000rpm$, then coasts at a steady angular velocity for another $0.500s$.
    $$
    \omega=209.rad/s=33.\overline3rev/s
    $$
    

    1. What is the disk's angular acceleration during the first $0.500s$ interval?
        $$
        \omega_{0.5s}=\alpha(\Delta t)\\
        \alpha=\frac{\omega_{0.5s}}{t}\\
        \alpha=\frac{209.rad/s}{0.500s}\ |\alpha=\frac{33.\overline3rev/s}{0.500s}\\
        \alpha=418.8790205\ rad/s^2\ |\alpha=66.\overline6rev/s^2\\
        \overline{\underline{|\alpha=419.\ rad/s^2|}}
        $$
        
2. Through how many radians has it turned at time $0.500s$? How many revolutions does this correspond to?
        $$
        \theta=\frac12\alpha t^2\\
        \theta=\frac12(419.rad/s^2)(0.5s)^2\ |\ \theta=\frac12(66.\overline6\ rev/s^2)(0.5s)^2\\
        \theta=52.35987759rad\ |\theta=8.33\overline3rev\\
        \\\overline{\underline{|\theta=52.4rad=8.33\ revolutions|}}
        $$
        
    3. Through how many radians has it turned at time $1.00s$? How many revolutions does this correspond to?
    $$
        \theta_f=\theta_{0.5s}+\omega t\\
        \theta_f=52.4rad+209.rad/s*0.500s\ |\theta_f=8.33rev+33.\overline3rev/s*0.500s\\
        \overline{\underline{|\theta=157.rad=25.0rev|}}
        $$
        
    4. What is the disk's angular velocity $\omega$ at $t=1.00s$?
        $$
    \omega_{1.00s}=209.rad/s=33.3rev/s
        $$
        
    5. Sketch the angular acceleration, velocity, and position for $0\le t\le1.00s$. Assume the initial angular position of the disk is zero.
        ![image-20201019163026602](macdougall_skyler_week9.assets/image-20201019163026602.png)
    
2. A vinyl record in an old fashioned record player always rotates at the same angular speed. CDs, by contrast, must have a constant tangential speed of $v_t=1.25m/s$ relative to the laser to read correctly.

    1. As a CD plays from the center outward, does the angular speed change?
        Yes, it decreases. 

    2. What is the angular speed of the CD when the laser beam is $2.50cm$ from the center?
        $$
        \omega=\frac{v_t}{r}=\frac{1.25m/s}{2.50cm/rad}\\
        \omega=50rad/sec\\
        \overline{\underline{|\omega=50.0rad/s|}}
        $$
        
3. What is the angular speed of the CD when the laser beam is $6.00cm$ from the center?
        $$
        \omega=\frac{v_t}{r}=\frac{1.25m/s}{6.00cm/rad}\\
        \omega=20.8\overline3rad/sec\\
        \overline{\underline{|\omega=20.8rad/s|}}
        $$
        
    4. If the CD plays for $66.5min$ and the laser beam moves radially outward from $2.50cm$ to $6.00cm$ during this time, what is the magnitude of the CD's average angular acceleration?
    $$
        \alpha=\frac{\Delta\omega}{t}\\
        \alpha=\frac{20.8rad/s-50rad/s}{66.5min}\\
        \alpha=-7.309941520\times10^{-3}rad/s^2\\
        \overline{\underline{|\alpha=-7.31\times10^{-3}rad/s^2|}}
        $$
        
    
3. A small rubber wheel is used to drive a large pottery wheel. The two wheels are mounted so that their circular edges touch. The small drive-wheel has a radius of $2.20cm$ and accelerates at the rate of $8.00rad/s^2$, and it is in contact with the pottery wheel, who has a radius of $28.0cm$. Both wheels move without slipping. The rubber wheel rotates clockwise.

    1. Find the angular acceleration of the large pottery wheel.
        Rotating wheels/circles/cylinders/etc that touch, assuming only one has an active force acting on it, will rotate in opposite directions.
        $$
        r_d\alpha_d=-r_p\alpha_p;\ r_d=2.20cm;\ r_p=28.0cm;\ \alpha_d=8.00rad/s^2\\
        \alpha_p=-\frac{r_d\alpha_d}{r_p}\\
        \alpha_p=-\frac{(2.20cm)(-8.00rad/s^2)}{28.0cm}\\
        \alpha_p=0.6\overline{285714}rad/s^2\\
        \overline{\underline{|\alpha_p=0.629rad/s^2|}}
        $$
        
2. Calculate the time it takes the pottery wheel to reach its required speed of $60rpm$, if both wheels start from rest. 
        $$
        60rpm=6.283185307...rad/s\\
        \alpha=\frac{\omega}{t};\ t=\frac{\omega}{\alpha}\\
        t=\frac{6.283185307...rad/s}{0.6\overline{285714}rad/s^2}\\
        t=9.995976625s\\
        t=10s
        $$
        