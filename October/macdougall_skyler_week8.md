# Week 8 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/9/2020

# Law of Universal Gravitation

1. At new moon, the earth, moon, and sun are in a straight line.
    $$
    Givens:\\
    M_{earth}=5.67\times 10^{24}kg\\
    M_{moon}=7.35\times10^{22}kg\\
    M_{sun}=2.00\times10^{30}kg\\
    d_{moon-earth}=3.84\times10^5km\\
    AU=1.50\times10^8km
    $$
    

    1. What is the net gravitational force exerted on the earth by the moon and the sun?
        $$
        F_g=G\frac{mM}{r^2}\\
        F_{net}=F_{g1}+F_{g2}\\
        F_g=G\frac{mM}{r^2}+G\frac{mM}{r^2}\\
        F_g=Gm(\frac{M}{r^2}+\frac{M}{r^2})\\
        F_g=G(5.67\times 10^{24}kg)(\frac{2.00\times10^{30}kg}{(1.50\times10^8km)^2} +\frac{7.35\times10^{22}kg} {(3.84\times10^5km)^2})\\
        F_g=G(5.67\times 10^{24}kg)(8.938734266\times10^7 kg ∕ m^2)\\
        F_g=3.383\times10^{22} N\\
        \overline{\underline{|F_g=3.38\times10^{22}N\ towards\ the\ sun|}}
        $$

    2. ... on the moon by the sun and the earth?
        $$
        F_g=Gm(\frac{M}{r^2}-\frac{M}{r^2})\\
        F_g=G(7.35\times10^{22}kg)(\frac{2.00\times10^{30}kg}{(1.50\times10^8km-3.84\times10^5km)^2} -\frac{5.67\times 10^{24}kg} {(3.84\times10^5km)^2})\\
        F_g=G(5.67\times 10^{24}kg)(5.089360517\times10^7 kg ∕ m^2)\\
        F_g=2.497\times10^{20} N\\
        \overline{\underline{|F_g=2.497\times10^{20} N\ towards\ the\ sun|}}
        $$
        
    3. ... on the sun by the moon and the earth?
    
    $$
        F_g=Gm(\frac{M}{r^2}+\frac{M}{r^2})\\
        F_g=G(2.00\times10^{30}kg)(\frac{7.35\times10^{22}kg}{(1.50\times10^8km-3.84\times10^5km)^2} +\frac{5.67\times 10^{24}kg} {(3.84\times10^5km)^2})\\
        F_g=G(5.67\times 10^{24}kg)(3.845215172\times10^7 kg ∕ m^2)\\
        F_g=5.133\times10^{27} N\\
        \overline{\underline{|F_g=5.133\times10^{27} N\ towards\ the\ sun|}}
    $$
    
2. When the earth, moon, and sun form a right triangle, with the moon located at the  right angle, the moon is in its third quarter phase. What is the net gravitational force on the moon?
    $$
    F_{g}=G\frac{mM}{r^2}+G\frac{mM}{r^2}\\
    F_{g}=G\frac{(7.35\times10^{22}kg)(2.00\times10^{30}kg)}{(1.50\times10^8km)^2-(3.84\times10^5km)^2}\ang180^\circ+G\frac{(7.35\times10^{22}kg)(5.67\times10^{24}kg)}{(3.84\times10^5km)^2}\ang270^\circ\\
    F_{g}=1.910534752\times10^{20}N\ang260.8078552^\circ\\
    \overline{\underline{|F_g=1.91\times10^{20}N\ang261^\circ|}}
    $$
    
4. An asteroid has a mass of $3.35\times10^{15}kg$ and a radius of $19.0km$. Assume the asteroid is a uniform sphere.

    1. What is the magnitude of gravitational acceleration on the surface of the asteroid?
        $$
        F_g=G\frac{mM}{r^2}\\
        a=\frac{GM}{r^2}\\
        a=\frac{G(3.35\times10^{15}kg)}{(19.0km)^2}\\
        \overline{\underline{|a=6.19\times10^{-4}m/s^2|}}
        $$
        
    2. With what speed must you launch a rocket of mass $m$ so that it completely escapes the asteroid's gravitational pull?
        $$
        v_e=\sqrt{\frac{2GM}{r}}\\
        v_e=\sqrt{\frac{2G(3.35\times10^{15}kg)}{19.0km}}\\
        v_e=4.851m/s\\
        \overline{\underline{|v_e=4.85m/s|}}
        $$
        

5. The largest moon in the solar system is Ganymede, a moon of Jupiter. Ganymede orbits at a distance of $1.07\times10^9m$ from the center of Jupiter, with an orbital period of $6.18\times10^5s$. Using this information, calculate the mass of Jupiter.
    $$
    v_o=\sqrt{\frac{GM}{r}}\\
    v=\frac dt=\frac{2\pi r}t=\frac{2\pi (1.07\times10^9m)}{6.18\times10^5s}\\
    v_o=1.527949235\times10^4m/s\\
    \frac{v_o^2r}{G}=M\\
    M=\frac{(1.527949235\times10^4m/s)^2(1.07\times10^9m)}{G}\\
    M=3.743\times10^{27}kg\\
    \overline{\underline{|M=3.74\times10^{27}kg|}}
    $$
    

# Linear Momentum

1. A puck of mass $m_1=0.151kg$ slides along a frictionless horizontal surface with a velocity of $\overrightarrow{v_{1i}}=0.450m/s$ directed horizontally to the right. This puck makes a glancing collision with a second puck of mass $m_2=0.250kg$ sliding with a velocity $\overrightarrow{v_{2i}}=0.135m/s$ directed horizontally to the left. After the collision, $m_1$ is observed to be moving with a speed $\overrightarrow{v_{1vf}}=0.355m/s$ at an angle of $\theta_{1f}=17.1^\circ$.

    1. What is the velocity of puck $m_2$ after the collision?
        $$
        p_i=p_f\\
        m_1v_{1i}+m_2v_{2i}=m_1v_{1f}+m_2v_{2f}\\
        v_{2f}=\frac{m_1v_{1i}+m_2v_{2i}-m_1v_{1f}}{m_2}\\
        v_{2f}=\frac{(0.151kg)(0.450m/s\ang0^\circ)+(0.250kg)(0.135m/s\ang180^\circ)-(0.151kg)(0.355m/s\ang17.1^\circ)}{0.250kg}\\
        v_{2f}=0.009283469707m/s\ang-137.2232125^\circ\\
        \overline{|0.0928m/s\ang-137.^\circ|}\\\underline{|0.0928m/s\ang137.^\circ clockwise|}
        $$
        
    2. Is the collision elastic? If not, calculate the fractional change in the kinetic energy ($\frac{\Delta K}{K_i}=\frac{K_f-K_i}{K_i}$) .
        $$
        Elastic:\Delta K=0\\
        \Delta K=K_f-K_i\\
        \Delta K=\frac12(m_{1}v_{1i}^2+m_{2}v_{2i}^2-(m_{1}v_{1f}^2+m_{2}v_{2f}^2))\\
        \Delta K=\frac12((0.151kg)(0.450m/s\ang0^\circ)^2+(0.250kg)(0.135m/s\ang180^\circ)^2-...\\
        ((0.151kg)(0.355m/s\ang17.1^\circ)^2+(0.250kg)(0.0928m/s\ang-137^\circ)^2))\\
        \Delta K=0.01156154894J\\
        \Delta K\ne0J\\\therefore\\inelastic\\
        \frac{\Delta K}{K_i}=\frac{\frac12(m_{1}v_{1i}^2+m_{2}v_{2i}^2-(m_{1}v_{1f}^2+m_{2}v_{2f}^2))}{\frac12(m_{1}v_{1f}^2+m_{2}v_{2f}^2)}\\
        \frac{\Delta K}{K_i}=\frac{0.01156154894J}{((0.151kg)(0.355m/s\ang17.1^\circ)^2+(0.250kg)(0.0928m/s\ang-137^\circ)^2))}\\
        \frac{\Delta K}{K_i}=\frac{0.01156154894J}{0.01022235025J}\\
        \frac{\Delta K}{K_i}=1.131006927\\
        \overline{\underline{|\frac{\Delta K}{K_i}=1.13|}}
        $$
        
    
2. A block of mass $m_1$ is attached to a horizontal spring that is at equilibrium. The block rests on a frictionless horizontal surface. A wad of putty mass $m_2$ is thrown horizontally at the block, hitting it with a speed of $v_{2i}$ and sticking to the block.

    1. Derive an expression for the maximum amount that the putty-block system compresses the spring after the collision.
        $$
        U_{spring}=\frac12kx^2;\ K=\frac12mv^2;\ p_i=p_f\\
        m_2v_{2i}=m_{1+2}v_{f}\\
        v_f=\frac{m_2v_{2i}}{m_{1+2}}\\
        kx^2=mv^2\\
        x^2=\frac{mv^2}{k}\\
        x=\sqrt{\frac{m_{1+2}(\frac{m_2v_{2i}}{m_{1+2}})^2}{k}}
        $$
        
2. Determine the compression distance given the following:
        $$
        m_1=0.430kg\\
        m_2=0.0500kg\\
        k=20.0N/m\\
        v_{2i}=2.330m/s
        $$
    
$$
    x=\sqrt{\frac{m_{1+2}(\frac{m_2v_{2i}}{m_{1+2}})^2}{k}}\\
    x=\sqrt{\frac{(0.430kg+0.500kg)(\frac{(0.500kg)(2.330m/s)}{0.430kg+0.500kg})^2}{20.0N/m}}\\
    x=0.2701279068m\\
    \overline{\underline{|x=0.270m|}}
    $$
    

    
3. Block A in the figure below has a mass $m_A=1.50kg$, and block B has a mass $m_B=3.00kg$. The blocks are forced together, compressing a spring between them. The system is released from rest on a level, frictionless surface. The spring which has negligible mass, is not attached to either block, and it drops to the surface after it has expanded and pushed on both blocks. Block B acquires a speed of $1.25m/s$. 

    1. What is the final speed of block A?
        $$
        p_i=p_f\\
        p_i=0\\
        m_Av_A=-m_Bv_B\\
        v_A=\frac{m_Bv_B}{m_A}\\
        \frac{m_B}{m_A}=2\\
        \overline{\underline{|v_A=2.50m/s|}}
        $$
        
2. How much potential energy was stored in the compressed spring?
        $$
        P=K\\
        K=\frac12(m_Av_A^2+m_Bv_B^2)\\
        P=\frac12((1.50kg)(2.50m/s)^2+(3.00kg)(1.25m/s)^2)\\
        P=63.28125J\\
        \overline{\underline{|P=63.3J|}}
        $$
        
    
4. A chunk of ice of mass $m_1$ is sliding with a horizontal velocity of magnitude $v_{1i}$ on the floor of an ice-covered valley when it collides with and sticks to another chunk of ice mass $m_2$ that is at rest at the base of a hill. The two blocks stick together and move up the hill together. Since the valley and the hill are icy, there is no friction between the chunks and the ground.

    1. Which conservation laws may be applied to understand the motion of the ice chunks in this problem?
        Conservation of energy and conservation of momentum.

    2. What is the maximum vertical distance $H$ that the combined ice chunks will go up the hill after the collision? [^1]
        $$
        p_f=p_i\\
        m_fv_f=m_iv_{1i}\\
        v_f=\frac{m_iv_{1i}}{m_f}\\
        v_f=\frac{m_1v_{1i}}{m_1+m_2}\\
        U=K\\
        mgh=\frac12mv^2\\
        H=\frac{(\frac{m_1v_{1i}}{m_1+m_2})^2}{2g}
        $$
        
3. Calculate the numerical value $H$ given:
        $$
        m_1=5.00kg\\
        m_2=7.50kg\\
        v_{1i}=12.0m/s
        $$
    
    $$
        H=\frac{(\frac{m_1v_{1i}}{m_1+m_2})^2}{2g}\\
        H=\frac{(\frac{5.00kg(12.0m/s)}{(5.00kg)+(7.50kg)})^2}{2(9.81m/s^2)}\\
        H=1.174311927m\\
        \overline{\underline{|H=1.17m|}}
        $$
        
        
        
    4. How much kinetic energy is lost due to the collision?
        $$
        \Delta K=K_f-K_i;\ K_f=U_f\\
        \Delta K=\frac12mv^2-mgh\\
        \Delta K=\frac12m_1v_{1i}^2-m_{1+2}gH\\
        \Delta K=\frac12(5.00kg)(12.0m/s)^2-(5.00kg+7.50kg)(9.81m/s^2)(1.17m)\\
        \Delta K=216J\\
        \overline{\underline{|\Delta K=216.J|}}
        $$
        
    
    
    
5. A Prius of mass $m_1=1.61\times10^3kg$ is traveling north with an unknown initial speed $v_{1i}$. A RAV4 of mass $m_2=1.61\times10^3kg$ is travelling with an initial velocity of $\overrightarrow{v_{2i}}=17.8m/s\ang 160^\circ$. The two vehicles collide annd stick together. Immediately after the collision, the combined vehicles are moving with a velocity of $\overrightarrow{v_f}=13.1m/s\ang133.4^\circ$. What is the initial speed of the prius before the collision?
    $$
    p_i=p_f\\
    m_1v_{1i}+m_2v_{2i}=m_{1+2}v_{f}\\
    v_{1i}=\frac{m_{1+2}v_{f}-m_2v_{2i}}{m_1}\\
    v_{1i}=\frac{((1.38+1.61)\times10^3kg)(13.1m/s\ang133.4^\circ)-(1.61\times10^3kg)(17.8m/s\ang160^\circ)}{1.38\times10^3kg}\\
    v_{1i}=13.51999850m/s\ang89.94724050^\circ\\
    \overline{\underline{|v_{1i}=13.5m/s|}}
    $$
    