# Inverted Lecture : Gravitation

## Skyler MacDougall

## PHYS-111 1pm Lecture

#### Due: 10/9/2020

1. In one short sentence, explain why we call the force of gravity an attractive force.
    Gravity is an attractive force because it is a force that "pulls" one object to another.

2. Does gravity exist between any two objects?

    1. How does the force of gravity depend on the distance between the two objects?
        The force of gravity is proportional to the inverse square of the distance between the two objects.
    2. Is the gravitational attraction between the two objects the same magnitude for each object? Is it the same direction for each object?
        The magnitude between the two objects is the same. The direction is directly opposite of the other's direction. Or, in other words, the gravitational attraction force vectors point towards each other.

3. If there's more than one gravitational force acting on a body, can you just add the magnitudes of the gravitational forces acting on that body? If not, state why you cannot, and what mathematical tools you would then have to use to find the total gravitational force acting on a body.
    You cannot simply add the magnitudes of the gravitational forces acting on that body. The gravitational force vectors must be added to properly represent the gravitational forces acting on an object.

4. Write the formula for the magnitude of the gravitational force for two bodies of masses $m$ and $M$ whose centers are a distance $r$ apart.
    $$
    F_g=G\frac{mM}{r^2}
    $$

5. As you already know, the force of gravity acting on a body of mass $m$ is also given by the formula $F_g=w=mg$, where $g$ is the acceleration due to gravity. Put the two formulas for the force of gravity on earth equal to each other to derive an equation for the acceleration due to gravity on any planet of mass $M_p$ and radius $R_p$.
    $$
    ma=G\frac{mM}{r^2}\\
    a=G\frac{M_p}{R_p^2}\\
    $$

6. Describe in one short sentence how an object in orbit experiences weightlessness even though the force of gravity is always acting on it.
   
7. In one short sentence, explain how circular space stations in sci-fi movies create artificial gravity.
    In space, there is (effectively) no forces acting on you. As such, you are "weightless". Circular space stations create artificial gravity by spinning the station. This spin can be kept going essentially indefinitely, due to the lack of air resistance, and it causes the objects within the space ship to accelerate out towards the outer radius of the spaceship. When the spaceship is spinning at the correct speed, the force due to this acceleration is equal to the force due to gravity on earth.

8. Using the concepts of a projectile's trajectory, of the planet's curvature, and of free fall, answer in one or two short sentences the following question:  "If gravity is always pulling on the moon, why doesn't the moon fall into the Earth?"
    The moon is in an orbit. As such, it can be thought of as a circular motion problem, with the force of gravity being the acceleration on the moon. The force of gravity does not have enough of an acceleration to modify the velocity vector of the moon so that the velocity vector falls in line with the earth.

9. What are Kepler's 3 laws of planetary motion? State Laws 1 and 2, and write the formula for Law 3.
    Law 1: A planet orbits its star about an eclipse, with the sun at one focus of the eclipse.
    Law 2: A line connecting a planet to its star sweeps out equal areas in equal times as the planet moves around its orbit. That is, the planet moves faster when it is closer to its star.
    Law 3:
    $$
    t_{orbit}=Ar^{\frac32}
    $$
    , where $A$ is some constant.

10.  

    1. In one short sentence, describe "orbital speed".
        Orbital speed is the speed necessary to achieve a stable orbit around an object. 

    2. Derive the formula for orbital speed by starting with the general formula for the force of gravity and the formula for newton's second law of motion.
        $$
        F_g=G\frac{Mm}{r^2};\ F_{net}=m\frac {v^2}{r}\\
        G\frac{Mm}{r^2}=m\frac{v^2}r\\
        G\frac{M}{r}={v^2}\\
        v=\sqrt{\frac{GM}{r}}
        $$
        
3. What is the condition for this formula of orbital speed to reduce it to $v=\sqrt{rg}$?
        When the orbit is very close to the surface of the object being orbited.
    
11. In one sentence, describe what geosynchronous satellites and geostationary satellites are.
    Both geosynchronous satellites and geostationary satellites are satellites that orbit an object at a synchronous speed. Geostationary satellites orbit around the same pole as the object they're orbiting, while geosynchronous satellites can orbit with an inclination. That is, the pole of the geosynchronous orbit is not necessarily the same as the pole of the object's rotation. 

12.  

    1. Write the general equation for the Earth's gravitational potential energy (it includes the dependence on the distance from the center of the earth, $r$).
        $$
        U=-G\frac{mM_E}{r}
        $$
        
2. With respect to the surface of the earth, where is the approximation $U=mgh$ applied?
    
    When the difference between the radius of the earth and the object's distance from the center of the earth is close to the radius of the earth.
    
13.  

    1. Describe in one short sentence what "Escape speed" is.
        Escape speed is the speed necessary to escape the pull of an object's gravity.

    2. Starting wit the conservation of energy equation, $\Delta K+\Delta U=0$, derive an equation for the escape speed, $v_e$, of any object of mass $m$ from any planet of mass $M_p$ and radius $R_p$. 
        $$
        K+U=0\\
        \frac12mv_e^2+\frac{-GMm}{r}=0\\
        v_e=\sqrt{\frac{2GM}{r}}
        $$
        
3. Now, as an example, calculate the escape speed from Earth, given the following:
        $$
        M_E=5.98\times10^{24}kg\\
        R_E=6380km\\
        G=6.67\times10^{-11}Nm^2kg^{-2}
        $$
    
$$
    v_e=\sqrt{\frac{2GM}{r}}\\
    v_e=\sqrt{\frac{2(6.67\times10^{-11}Nm^2kg^{-2})(5.98\times10^{24}kg)}{6380km}}\\
    v_e=11.19\times10^3m/s\\
    \overline{\underline{|v_e=1.12\times10^4m/s|}}
    $$
    

    
14. Put the "orbital speed" and "escape speed" formulae next to each other, and circle the differences.
    $$
    v_{orbit}=\sqrt{\frac{GM}{r}}\\
    v_e=\sqrt{\frac{\underline{\overline{|2|}}GM}{r}}
    $$
    
15.  

    1. In one short sentence, describe what a "black hole" is.
        A black hole is an object with (theoretically) infinite mass and an infinitely small diameter. 

    2. Use plausible numbers for the mass and radius of a black hole like the one at the center of our Galaxy and calculate the escape speed from your black hole. Use this to show why, "Nothing escapes a black hole, not even light".
        According to [*Astronomy & Astrophysics*](https://www.aanda.org/articles/aa/full_html/2019/05/aa35656-19/aa35656-19.html), the mass of the black hole at the center of our galaxy is $(4.154\pm0.014)\times10^6M_\odot$. $M_\odot$ is the mass of our Sun, at $9.9891\times10^{30}kg$, according to [NASA](https://web.archive.org/web/20100715200549/http://nssdc.gsfc.nasa.gov/planetary/factsheet/sunfact.html). The radius of a black hole, named the Schwarzschild radius, is calculated based off the mass, using the following formula:
        $$
        r=\frac{2GM}{c^2}
        $$
        Therefore.
        $$
        v_e=\sqrt{\frac{2GM}{r}};\ r=\frac{2GM}{c^2}\\
        v_e=\sqrt{\frac{2GM}{\frac{2GM}{c^2}}}\\
        v_e=\sqrt{\frac{1}{\frac{1}{c^2}}}=c
        $$
        The velocity required to escape a black hole is the speed of light. 

16. Briefly describe a concept that surprised or interested you the most in this chapter.
    $GM$ is often abbreviated in the astronomy/astrophysics realm to $\mu$, which kinda makes sense, because you don't tend to use micro- when you're working at the scale of the universe.