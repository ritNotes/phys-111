# Week 5 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 9/25/2020

# Hooke's Law

1. A spring with a force constant $k$ is used to push a block of wood of mass $m$ against a wall as shown in the diagram. The coefficient of static friction between the block and the wall is $\mu_s$

    ![image-20200921145147462](macdougall_skyler_week5.assets/image-20200921145147462.png)

    1. What is the minimum amount of compression of the spring needed to keep the block from falling? [^1]
        $$
        \overrightarrow {F_{static}}=\mu_s\overrightarrow {F_N}\\
        \overrightarrow {F_N}=\overrightarrow {F}\\
        \overrightarrow F=-kx\\
        \overrightarrow {F_{static}}=\overrightarrow W\\
        \overrightarrow {W}=mg\\
        \therefore\\
        mg=\mu_s(-kx)\\
        -\frac{mg}{\mu_sk}=x
        $$
    
2. Calculate the numerical value for the minimum amount of compression if the spring needed to keep the block from falling if:
        $$
        k=120N/m\\
        m=0.270kg\\
        \mu_s=0.460
        $$
    
    $$
        -\frac{mg}{\mu_sk}=x\\
        -\frac{(0.270kg)(-9.81m/s^2)}{0.460(120N/m)}=x\\
        -\frac{(0.270)(-9.81)N}{0.460(120)N/m}=x\\
        -\frac{(0.270)(-9.81)}{0.460(120)}m=x\\
        \frac{(0.270)(9.81)}{0.460(120)}m=x\\
        x=0.047983696m\\
        \overline{\underline{|x=48.0mm|}}
    $$
    
    
    ​    
    3. Does your answer for part 1.1 change if the mass of the block of wood is doubled? Why?
        The answer is dependent on the mass proportionally, so if the mass of the block of wood doubles, the spring compression distance must also double.
    
2. A block of mass $m$ rests on a rough plank that is inclined at an angle $\theta$ above the horizontal. The upper end of the box is attached to a spring with force constant k, as shown in the diagram. The coefficient of static friction between the box and the plank is $\mu_s$.

    1. Draw the free body diagram for the block.

        ![image-20200921152216504](macdougall_skyler_week5.assets/image-20200921152216504.png)

    2. What is the maximum amount the spring can be stretch and the box remain at rest? [^1]
        $$
        \overrightarrow{F_{normal}}=\overrightarrow W\sin(270-\theta)\\
        \overrightarrow {F_{spring}}=-kx\\
        \overrightarrow {F_{friction}}=\mu_s\overrightarrow {F_{normal}}\\
        \overrightarrow {W}=mg\\
        \overrightarrow {F_{friction}}=\overrightarrow{F_{spring}}\\
    \therefore\\
        -kx=\mu_s((mg)\sin(270-\theta))\\
    x=-\frac{\mu_s((mg)\sin(270-\theta))}{k}
        $$
        
    3. Calculate the maximum amount of spring spring displacement given:
        $$
        m=2.00kg\\
        \theta=65.0^\circ\\
        k=360N/m\\
        \mu_s=0.220
        $$
    
        $$
        x=-\frac{\mu_s((mg)\sin(270-\theta))}{k}\\
        x=-\frac{0.220(((2.00kg)(-9.81m/s^2))|\sin(270-65.0^\circ)|)}{360N/m}\\
        x=0.0050671930m\\
        \overline{\underline{|x=5.07mm|}}
        $$
        
    4. Does your answer to part 2.3 change if the mass of the box increases? Why?
        The answer is proportional to the mass, so if the mass increases, the answer will also increase. 
    
3. A spring is attached to a rotating mass $m$ that is moving counterclockwise in a circle of radius $R$ at a constant speed $v$ on a smooth surface. During this motion, the spring is stretched a distance $x$ from its equilibrium length. ($x<R$). 

    1. Draw a free body diagram.![image-20200922151737381](macdougall_skyler_week5.assets/image-20200922151737381.png)

    2. Find an expression for the force constant of the spring. [^1]
        $$
        \overrightarrow{F_{net}}=ma_c=\frac{mv^2}{r}\\
        \overrightarrow{F_{net}}=\overrightarrow{F_{spring}}=-kx\\
        -kx=\frac{mv^2}{r}\\
        k=-\frac{mv^2}{Rx}
        $$
        
    3. Determine the value of the spring constant given:
        $$
        m=(200\pm1)g\\
        v=(1.40\pm0.03)m/s\\
        R=(13.0\pm0.4)\times10^{-3}m\\
        x=(4.0\pm0.1)\times10^{-3}m
        $$
        
   
    $$
        k=-\frac{mv^2}{Rx}\\
        k=-\frac{((0.200\pm0.001)kg)*((1.40\pm0.03)m/s)^2}{((13.0\pm0.4)\times10^{-3}m)*((4.0\pm0.1)\times10^{-3}m)}\\
        k=-\frac{((0.200\pm0.001))*((1.40\pm0.03))^2}{((13.0\pm0.4)\times10^{-3})*((4.0\pm0.1)\times10^{-3})}\frac{kgm^2/s^2}{m^2}\\
        k=-\frac{(0.200\pm0.001)*(1.40\pm0.03)^2}{(13.0\pm0.4)*(4.0\pm0.1)*10^{-6}}{kg/s^2}\\
        k=-\frac{(0.200\pm0.001)*(1.40\pm0.03)^2}{(13.0\pm0.4)*(4.0\pm0.1)*10^{-6}}N\\
        k=-\frac{(0.200)*(1.40)^2}{(13.0)*(4.0)*10^{-6}}N=-7.5384615kN\\
        k=-\frac{(0.201)*(1.43)^2}{(13.4)*(4.1)*10^{-6}}N=-7.4813415kN\\
        \overline{\underline{|k=-(7.54\pm0.06)kN=-(7.54\pm0.06)\times10^3N|}}
    $$


# Circular Motion

1. A car of mass $m$ drives through a curve on a horizontally flat road at a constant speed $v$. The curve has a radius of curvature $R$.

    1. Draw a free body diagram for the car. (Side View)

        ![freeBodyDiagram](macdougall_skyler_week5.assets/freeBodyQuestion6.png)

    2. What is the magnitude of the frictional force that keeps the car moving in a circle of radius $R$ with a uniform speed $v$? [^1]
        $$
        \overrightarrow {F_{net}}=ma=\frac{mv^2}{r}\\
        \overrightarrow {F_{net}}=\overrightarrow {F_{friction}}\\
        \therefore\\
        \overrightarrow {F_{friction}}=\frac{mv^2}{r}
        $$
        
    3. If the car has a mass of $1500kg$, then what is the magnitude of the frictional force acting on the car that keeps it moving in a circle with a radius of $50.0m$ with a uniform speed of $15.0m/s$.
        $$
        \overrightarrow {F_{friction}}=\frac{mv^2}{r}\\
            \overrightarrow {F_{friction}}=\frac{(1500kg)(15.0m/s)^2}{50.0m}\\
            \overrightarrow {F_{friction}}=\frac{(1500)(15.0)^2}{50.0}\frac{kgm^2/s^2}{m}\\
            \overrightarrow {F_{friction}}=\frac{(30\cancel{1500})(15.0)^2}{\cancel{50.0}}{kgm/s^2}\\
            \overrightarrow {F_{friction}}={(30)(15.0)^2}N\\
            \overrightarrow {F_{friction}}={(30)(15.0)^2}N\\
            \overrightarrow {F_{friction}}=6750N\\
            \overline{\underline{|\overrightarrow {F_{friction}}=6.75kN|}}
        $$
        ​    
    
    4. If the coefficient of static friction between the tires and the road is $\mu_s$, find the safe speed a car can pass through the curve without sliding. [^1]
    
    $$
        \overrightarrow {F_{friction}}=\frac{mv^2}{r}\\
        \overrightarrow {F_{friction}}=\mu_s\overrightarrow{F_{normal}}\\
        \overrightarrow {F_{normal}}=mg\\
        \mu_s(mg)=\frac{mv^2}{r}\\
        \sqrt{\frac{\mu_s(mg)r}{m}}=v\\
        \sqrt{{\mu_sgr}}=v\\
    $$
    
    
    5. Does the maximum safe speed increase or decrease on:
            1. An icy road?
                Decrease.
                2. A tighter curve?
                Decrease.
                    3. A small car?
                No change.
    
2. A conical pendulum is formed by attaching a mass $m$ to a string of length $l$, then allowing the mass to swing in a horizontal circle. The string makes a constant angle $\theta$ with respect to the vertical. As a result, the mass moves with constant speed in a horizontal circle.

    1. Draw a free body diagram. 

        ![freeBodyDiagram](macdougall_skyler_week5.assets/freeBodyQuestion7.png)

    2. What forces in your free body diagram keep the mass moving in a circle at a constant speed?
        The x component of tension.

    3. Apply Newton's Second Law, and determine an expression for the magnitude of the tension in the string. [^1]
        $$
        \overrightarrow{F_{net}}=\overrightarrow{F_{tension}}\cos(90^\circ-\theta)\\
        \overrightarrow{F_{tension}}=\frac{\overrightarrow{W}}{\sin(90^\circ-\theta)}\\
        \overrightarrow{W}=mg\\
        \overrightarrow{F_{net}}=mg\cot(90^\circ-\theta)
        $$
        
    4. Determine an expression for the speed of the mass. [^1]
        $$
        \overrightarrow{F_{net}}=\frac{mv^2}{r}\\
        \sqrt{\frac{\overrightarrow{F_{net}}r}{m}}=v\\
        \sqrt{\frac{mg\cot(90^\circ-\theta)r}{m}}=v\\
        \sqrt{g\cot(90^\circ-\theta)r}=v\\
        r=l\cos(90^\circ-\theta)\\
        \sqrt{g\cot(90^\circ-\theta)(l\cos(90^\circ-\theta))}=v
        $$
    
    5. Find the tension and the speed, given the following:
    
    $$
        m=500g\\
        l=1.00m\\
        \theta=11.5^\circ
    $$
    
    $$
    \sqrt{g\cot(90^\circ-\theta)(l\cos(90^\circ-\theta))}=v;\ \overrightarrow{F_{net}}=mg\cot(90^\circ-\theta)\\
    \sqrt{(9.81m/s^2)\cot(90^\circ-11.5^\circ)((1.00m)\cos(90^\circ-11.5^\circ))}=v;\ \overrightarrow{F_{net}}=(0.500kg)(9.81m/s^2)\cot(90^\circ-11.5^\circ)\\
    \sqrt{(9.81m/s^2)\cot(78.5^\circ)((1.00m)\cos(78.5^\circ))}=v;\ \overrightarrow{F_{net}}=(0.500kg)(9.81m/s^2)\cot(78.5^\circ)\\
    0.63080258m/s=v;\ \overrightarrow{F_{net}}=0.99793353N\\
    \overline{\underline{|v=63.1cm/s;\ \overrightarrow{F_{net}}=998.mN|}}
    $$
    
    
    
3. In another type of amusement ride, passengers stand inside a cylinder with their backs against the wall. The cylinder has a diameter of $D$. The cylinder begins to rotate around the vertical axis. Once the cylinder reaches its operational rotational speed, the linear speed of a passenger is $v$ and the floor on which the passengers are standing drops away. If all goes well, the passengers will "stick" to the wall and not slide down. Let $m$ be the mass of the passenger. 

    1. Draw a free body diagram.

        ![freeBodyDiagram](macdougall_skyler_week5.assets/freeBodyLastQuestion.png)

    2. Determine an expression for the minimum coefficient of static friction required to keep a passenger from sliding down the cylinder's wall when the floor drops away. [^1]
        $$
        \overrightarrow{F_{friction}}=\mu_s\overrightarrow{F_{normal}}\\
        \overrightarrow{F_{friction}}=\overrightarrow{W}\\
        \overrightarrow{W}=mg\\
        \overrightarrow{F_{net}}=\frac{mv^2}r=\overrightarrow{F_{normal}}\\
        \therefore\\
        mg=\mu_s(\frac{mv^2}r)\\
        \frac{\cancel{m}gr}{\cancel{m}v^2}=\mu_s\\
        \frac{gr}{v^2}=\mu_s\\
        r=\frac D2\\
        \frac{gD}{2v^2}=\mu_s
        $$
        
    3. Calculate a numerical value for the coefficient of static friction, given:
        $$
        D=15.0m\\
        v=18.8m/s\\
        m=70.0kg
        $$
        
    
    $$
    \frac{gD}{2v^2}=\mu_s\\
    \frac{(9.81m/s^2)(15.0m)}{2(18.8m/s)^2}=\mu_s\\
    \frac{(9.81)(15.0)}{2(18.8)^2}=\mu_s\\
    \mu_s=0.20816829\\
    \overline{\underline{|\mu_s=0.208|}}
    $$
    
    
    
    
    



[^1]: Write the result in terms of given quantities and constants.