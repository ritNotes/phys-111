1. What is the general process for an approach of a physics problem?
    1. Read the question
        1. Understand (or find) the units. 
        2. Find the requested answer, and its associated units.
    2. Use provided equations. (These will be taught in class.)
    3. Convert units (may be optional)
    4. Ensure the answer makes sense.
    5. Reduce to required number of significant figures.
    6. Change answer to scientific notation (or metric prefixes, if allowed). (May be optional, depending on length of value)