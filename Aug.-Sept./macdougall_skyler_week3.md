# Week 3 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 9/11/2020

# 2-D Kinematics

1. A baseball is launched by a pitching machine on the ground at an angle of $30.0^\circ$ from the horizontal with an initial speed of $20.0m/s$.

    ![image-20200907151113320](macdougall_skyler_week3.assets/image-20200907151113320.png)

    1. How long does it take for the ball to hit the ground?
        $$
        v_i=20m/s\ang30^\circ\\
        v_{ix}\approx17.3m/s;\ v_{iy}=10m/s\\
        v_f=v_i+at\\
        0=10m/s+-9.81m/s*t\\
        t\approx1.02s\\
        2t\approx2.04s\\
        \overline{\underline{|t=2.04s|}}
        $$

    2. What is the horizontal range of the ball?
        $$
        d=vt\\
        d=17.3m/s*2.04s\\
        d=16.135m\\
        \overline{\underline{|d=16.1m|}}
        $$
        
    3. What is the maximum height?
        $$
        x=vt+\frac12at^2\\
        x=(10m/s)*1.02s+\frac12(-9.81m/s^2)(1.02s)^2\\
        \overline{\underline{|x=15.3m|}}
        $$
    
4. Sketch the horizontal and vertical components of the balls velocity.
   
    ![image-20200908114700619](macdougall_skyler_week3.assets/image-20200908114700619.png)
    
5. Sketch the horizontal and vertical components of the balls position.
   
    ![image-20200908114727947](macdougall_skyler_week3.assets/image-20200908114727947.png)
    
2. You want to throw a baseball over a wall that has a height of $10.0m$ and is $6.00m$ away from you. Assume that the wall is negligibly thick, and the baseball is small enough that you can consider it a point particle. Ignore air resistance.

    1. What is the minimum initial speed that the ball can have and still clear the wall? Assume that the minimum trajectory is the one in which the ball reaches the maximum height the instant it gets to the top of the wall.
        $$
        v_f^2=v_i^2+2ax\\
        (0m/s)^2=v_i^2+2(-9.81m/s^2)(10.0m)\\
        2(9.81m/s^2)(10.0m)=v_i^2\\
        v_i=14.01m/s\\
        v_f=v_i+at\\
        0m/s=14.01m/s+(-9.81m/s^2)t\\
        \frac{14.01m/s}{9.81m/s^2}=t\\
        t=1.43s\\
        d=vt\\
        6.00m=v(1.43s)\\
        \frac{6.00m}{1.43s}=v\\
        v=4.20m/s\\
        v_{total}=14.01m/s+i(4.20m/s)\\
        |v_{total}|=\sqrt{(14.01m/s)^2+(4.20m/s)^2}\\
        |v_{total}|=14.6m/s\\
        \ang V=tan^{-1}(\frac{4.20m/s}{14.01m/s})\\
        \ang V=16.7^\circ\\
        \overline{\underline{|v_{total}=14.6m/s\ang16.7^\circ|}}
        $$
    
2. If you throw the ball at this speed, what is the angle relative to the horizontal should you throw it?
        $$
        \theta=16.7^\circ
        $$
        
    
3. A snowball rolls off a barn roof that slopes downward at an angle of $40.0^\circ$ as shown in the figure. The edge of the roof is $14.0m$ above the ground, and the snowball has a speed of $7.00 m/s$ as it rolls off the roof. Ignore air resistance.

    ![image-20200907151147523](macdougall_skyler_week3.assets/image-20200907151147523.png)

    1. How far from the edge of the barn does the snowball strike the ground, if it doesn't strike anything else while falling?
        $$
        angle\ given=180^\circ-40^\circ\\
        \therefore\\
        v_0=-7.00m/s\ang140^\circ\\
        v_{0x}\approx5.36m/s;\ v_{0y}\approx-4.50m/s\\
        v_f^2=v_i^2+2ax\\
        v_f=\sqrt{(-4.5m/s)^2+2(9.81m/s^2)(14.00m)}\\
        v_f\approx-17.2m/s\\
        v_f=v_i+at\\
        -17.2m/s=-4.5m/s+(9.81m/s^2)(t)\\
        t=\frac{12.7m/s}{9.81m/s^2}\\
        t\approx1.29s\\
        d=vt\\
        d=5.36m/s*1.29s\\
        \overline{\underline{|d=6.93m|}}
        $$
    
2. A man $1.90m$ tall is standing $4.00m$ at the edge of the barn. Will he be hit by the snowball?
    $$
    d=vt\\
        4.00m=5.36m/s*t\\
        t=0.745s\\
        d=d_i+vt+\frac12at^2\\
        d=14.0m+(-4.5m/s)(0.745s)+\frac12(-9.81m/s^2)(0.745s)^2\\
        d=14.0m-((4.5m/s)(0.745s)+\frac12(9.81m/s^2)(0.745s)^2)\\
        d=7.91m\\
        7.91m>1.90m\\
        \therefore
    $$
    ​    The man will not get hit.
    
8. Sketch the graphs for position and velocity vs time.
    ![image-20200908121336160](macdougall_skyler_week3.assets/image-20200908121336160.png)
    ![image-20200908120504051](macdougall_skyler_week3.assets/image-20200908120504051.png)



# Forces and Free Body Diagrams

1. For each situation described, draw a free body diagram. Be sure to use standard notation for the forces and define your coordinate system.

    1. Your textbook is at rest on a horizontal table. 

        ![image-20200907160040681](macdougall_skyler_week3.assets/image-20200907160040681.png)

    2. You push vertically down on the book with a force $\overrightarrow {F_{push}}$. The book does not move.

        ![image-20200907160222877](macdougall_skyler_week3.assets/image-20200907160222877.png)

    3. A crate is held at rest above the floor by a single rope. 

        ![image-20200907160333504](macdougall_skyler_week3.assets/image-20200907160333504.png)

2. A block of weight W is at rest on a rough surface inclined at an angle $\theta$ with respect to the normal. Draw this free body diagram twice, defining axes that are parallel then perpendicular to the incline.

    ![image-20200907163138137](macdougall_skyler_week3.assets/image-20200907163138137.png)



# Ball Drop Pre-Lab

1. As part of the Ball Drop experiment, you will use LoggerPro to fit data for the position of the ball as a function of time during free fall to the quadratic equation.

    1. Write the SI units for each of the constants.
        $$
        A:m/s^2;\ B:m/s;\ C:m
        $$

    2. Name the physical quantity that has the same units as you wrote above for each of the constants.
        $$
        A: Acceleration;\ B:initial\ velocity;\ C:initial\ position
        $$

2. The vertical component of the velocity of a bouncing ball as a function of time is shown in the graph below. The positive y direction is vertically up. The ball deforms slightly while it is in contact with the ground.

    ![image-20200908121633283](macdougall_skyler_week3.assets/image-20200908121633283.png)
    
    1. Identify at least 2 non-zero instances of time at which the ball is at its maximum height. 
        $$
    t=2.25s;\ t=4.5s
        $$

    2. Calculate the acceleration of the ball while it is in the air. 
    $$
    a=\frac{\Delta v}{\Delta t}\\
        a=\frac{-9.0m/s-9.0m/s}{3.0s-1.0s}\\
        \overline{\underline{|a=-9.0m/s^2|}}
    $$
    
    3. Calculate the acceleration of the ball while it is in contact with the floor. 
        $$
        a=\frac{\Delta v}{\Delta t}\\
        a=\frac{9.0m/s-(-9.0m/s)}{1.25s-1.0s}\\
        \overline{\underline{|a=72.m/s^2|}}
        $$
        
    4. Using the area under the velocity time graph, calculate the maximum height above the floor that the ball reaches. Be sure that you only utilize the portion of the velocity time graph that corresponds to the ball in free fall.
        $$
        x_f=vt+\frac12at^2\\
        x_f=(9.0m/s)(1s)+\frac12(-9.0m/s^2)(1s)^2\\
        x_f=9.0m-4.5m\\
        \overline{\underline{|x_f=4.5m|}}
        $$
        