# Week 1 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group: Group 2

#### Due: 8/24/2020

# Unit Conversion

1. NASA's unpiloted, experimental X-43A "air breathing" test plane broke its own world aircraft speed record on its third and final flight in Nov. 16, 2004. The test plane, known as a "SCRAM jet", burns its own fuel - in this case, hydrogen - without the need to carry heavy tanks of oxidizer, as rockets must. Instead, it draws oxygen from the air, which is naturally compressed by the forward speed of the vehicle and the shape of the air inlet. Conventional jet engines have rotating blades to compress the air, SCRAM jets have no such moving parts. The test plane achieved a record speed of $11\times10^3km/hr$.
    1. Convert the SCRAM jet's record breaking speed to miles per hour. **SHOW ALL WORK.**
        $$
        \frac{11\times10^3\cancel{km}}{1\ hour}*\frac{1 mile}{1.609344\cancel{km}}=6835.0831miles/hour\approx\overline{\underline{|6.8\times10^3miles/hour|}}
        $$
    
    2. Convert this speed to nanometers per microsecond. **SHOW ALL WORK**
        $$
        \frac{11\times10^3\cancel{km}}{1\cancel{hour}}*\frac{1\times10^{12}nm}{1\cancel{km}}*\frac{1\ \cancel{hour}}{3600\ \cancel{second}}*\frac{1\ \cancel{second}}{1\times10^6\mu s}\\
        \frac{(11\times10^3)*(1\times10^{\cancel{12}6})}{1*(3600)*\cancel{(1\times10^6)}}nm/sec\\
        \frac{(11\cancel{\times10^3})*(1\times10^{6})}{\cancel{3600}3.6}nm/sec\\
        \frac{11\times10^6}{3.6}nm/sec=3.0\overline5\times10^6nm/sec\\
        \overline{\underline{|3.1\times10^6nm/sec|}}
        $$
    
2. Determine the area of a CD in $cm^2$. Convert the area to $km^2$. Assume the CD is a uniform disk of diameter 12.0cm with a central hole diameter of 1.5cm. **SHOW ALL WORK.**
    $$
    A_{circle}=\pi (\frac {D_{outer}-D_{inner}} 2)^2\\
    A_{circle}=\pi (\frac {12.0cm-1.5cm} 2)^2\\
A_{circle}=\pi (\frac {10.5cm} 2)^2\\
    \underline{\overline{|A_{circle}=86.59cm^2|}}\\
    \rule{125pt}{0.4pt}\\
    \frac{86.59cm^2}1*[\frac{1m}{100cm}]^2*[\frac{1km}{10^3m}]^2\\
    \frac{86.59\cancel{cm^2}}1*\frac{1\cancel{m^2}}{10^4\cancel{cm^2}}*\frac{1km^2}{10^6\cancel{m^2}}\\
    \frac{86.59}{10^{10}}km^2\\
    \underline{\overline{|A_{circle}=8.6\times10^{-9}km^2|}}
    $$
    
3. Density $\rho$ is defined as the ratio of mass to volume. The density of aluminum is $2702kg/m^3$. What is the density in $g/cm^3$? **SHOW ALL WORK.**
    $$
    \frac{2702\cancel{kg}}{1m^3}*\frac{10^3g}{1\cancel{kg}}*[\frac{1m}{100cm}]^3\\
    \frac{2702\cancel{kg}}{1\cancel{m^3}}*\frac{10^3g}{1\cancel{kg}}*\frac{1\cancel{m^3}}{10^6cm^3}\\
\frac{2072\cancel{*10^3}}{10^{\cancel{6}3}}g/cm^3\\
    \underline{\overline{|2.072g/cm^3|}}
    $$
    
4. The fuel efficiency of a car is described in the US by citing the number of ***miles per gallon*** (MPG) that it gets. In Europe they cite ***liters per 100 kilometers*** (LPK).
    $$
    Useful\ Unit\ Conversions\\
    1mi=1609m=1.609km\\
    1gal=3.785L
    $$
    
    1. Does a more fuel efficient car have a larger or smaller MPG number? Why?
        A more fuel efficient car has a larger MPG rating, because it can go a longer distance in the same amount of fuel.
        
    2. Does a more fuel efficient car have a larger or smaller LPK number? Why?
        A fuel efficient car has a smaller LPK rating, because it can use less gas in the same amount of distance.
        
    3. You are considering buying a gas-electric hybrid from an upstart European manufacturer that claims its hybrid gets 4.6LPK on the highway. Is this reasonable, or are you being "taken for a ride"? **Support your conclusion with calculations**.
       This is very fuel efficient.
       $$
       \frac{100km}{4.6L}*\frac{3.785L}{1gal}*\frac{1mi}{1.609km}\\
       \frac{100\cancel{km}}{4.6\cancel{L}}*\frac{3.785\cancel{L}}{1gal}*\frac{1mi}{1.609\cancel{km}}\\
       \frac{100*3.785}{4.6*1.609}mi/gal\\
       51.139mi/gal\approx\underline{\overline{|51mi/gal|}}
       $$
       

# Position-Time and Velocity-Time Graphs

1. A dog walks along a straight line, first in one direction, then in the opposite direction. Shown below are two possible *Position-Time* graphs for a dog's motion.

    ![image-20200824163606337](week1ActivitiesProblems.assets/image-20200824163606337.png)

    2. Which graph (Graph A or Graph B) more closely reflects the dog's movement? **Why**?
        Graph A represents the dogs positional movement, because it goes in one direction, then turns around.
    2. Explain why someone might choose the other graph. Also, describe the dog's motion if it were following this "other" graph.
        Someone would choose the other graph (Graph B) for the given explanation if they believed that it was a speed graph instead of a positional graph. Graph B actually represents a constant motion in one direction. 

2. A turtle moves along a line; first in one direction, then shown in the opposite direction. Shown below are two possible *Velocity-Time* graphs for the turtle's motion.

    ![image-20200824163638544](week1ActivitiesProblems.assets/image-20200824163638544.png)

    1. Which graph (Graph A or Graph B) more closely reflects the turtle's motion? **Why**?
        Graph B more closely reflects the turtle's motion, because the turtle has a negative speed first, then has a positive speed, so it changed directions.
    2. Explain why someone might choose the other graph. Also, describe the turtle's motion if it were following this "other" graph. 
        Someone would choose the other graph for the given explanation if they believed that it was a positional graph instead of a speed graph. Graph A represents a changing speed, but always positive. So, the turtle would start out slow, then speed up, then slow back down, but always travel in the same direction.

# Basic Measurements and Uncertainties

1. Suppose you measure the diameter of a sphere five times. Each of the values obtained is represented by the symbol $D_i$, where $D_1$ is the first measurement, $D_2$ is the second, etc. The average (mean) diameter is defined as:
    $$
    D_{AV}=\frac{\displaystyle\sum^{N}_{i=1}=D_i}{N}=\frac{D_1+D_2+D_3+D_4+D_5}{5}\\
    \delta D_{AV}=\frac{\displaystyle\sum^{N}_{i=1}=|D_i-D_{AV}|}{N}=\frac{|D_1-D_{AV}|+|D_2-D_{AV}|+|D_3-D_{AV}|+|D_4-D_{AV}|+|D_5-D_{AV}|}{5}
    $$
    The diameter of a sphere is measured five times using a digital caliper with a least count of $0.1mm$. The results are: $25.8mm$, $26.2mm$, $24.8mm$, $24.9mm$, $25.8mm$.

    1. Calculate the average and average deviation in the set of measurements.
        $$
        D_{AV}=\frac{\displaystyle\sum^{N}_{i=1}=D_i}{N}=\frac{D_1+D_2+D_3+D_4+D_5}{5}\\
        D_{AV}=\frac{25.8mm+26.2mm+24.8mm+24.9mm+25.8mm}{5}\\
        D_{AV}=\frac{127.5mm}5\\
        \overline{\underline{|D_{AV}=25.5mm|}}
        $$
    
        $$
        \delta D_{AV}=\frac{|D_1-D_{AV}|+|D_2-D_{AV}|+|D_3-D_{AV}|+|D_4-D_{AV}|+|D_5-D_{AV}|}{5}\\
        \delta D_{AV}=\frac{|25.8mm-25.5mm|+|26.2mm-25.5mm|+|24.8mm-25.5mm|+|24.9mm-25.5mm|+|25.8mm-25.5mm|}{5}\\
        \delta D_{AV}=\frac{2.6mm}{5}\\
        \overline{\underline{|\delta D_{AV}=0.5mm|}}
        $$
    
        
    
    2. What is the correct uncertainty to report for the diameter of the sphere?
        The correct uncertainty should be $\pm0.5mm$
    
    3. Write the diameter of the sphere in "proper form". 
        $(25.5\pm0.5)mm$
    
    4. The formula for the volume of a sphere is $V=\frac 1 6\pi D^3$, where D is the average (best) diameter of the sphere. $\delta D$ is the uncertainty in D, in this case, the average deviation in D. Using the results obtained by $D_{best}$ and $\delta D$, calculate the best estimate for the volume of the sphere and the uncertainty in the volume.
        $$
        V=\frac16\pi (D)^3;\ D_{best}=25.5mm;\ D_{max}=26.0mm\\
        V_{best}=\frac16\pi (25.5mm)^3;\ V_{max}=\frac16\pi (26.0mm)^3\\
        V_{best}=8.681\mu m^3;V_{max}=9.202\mu m^3\\
        \delta V=V_{best}-V_{max}=0.5207\mu m^3\\
        \overline{\underline{|V=(9.20\pm0.52)\mu m^3|}}
        $$
        Recall that the uncertainty is the calculated quantity is $\delta V=V_{max}-V_{best}$.
        Write the final result in "proper form".

# One Dimensional Kinematics Problems

1. The diagram below shows the position-time graph of a particle.

    ![image-20200824163512980](week1ActivitiesProblems.assets/image-20200824163512980.png)

    1. Draw the particle's velocity-time graph for the interval $0\le t\le4s$
       
        ![image-20200827150003322](week1ActivitiesProblems.assets/image-20200827150003322.png)
    2. Does the particle have a turning point or turning points? If so, at what time or times? (A turning point is a point at which the particle reverses its direction of motion)
       
        It does have a singular turning point, at 2 seconds.

2. A car starts from $x_i=10m,\ t_i=0$, and moves with the velocity time graph shown.

    ![image-20200824163533019](week1ActivitiesProblems.assets/image-20200824163533019.png)

    1. What is the object's position at $t=4s$? Show calculation.
       
        | Variable   | Value                            | Significance     |
        | ---------- | -------------------------------- | ---------------- |
        | $x_f,\ x$  | $90m$ (calculated)               | final position   |
        | $x_i$      | $10m$ (given)                    | initial position |
        | $v_{x_i}$  | $12m/s$ (given)                  | initial velocity |
        | $\Delta t$ | $4s-0s$ (given)                  | time traveled    |
        | $a_x$      | $4m/s^2$ (calculated from slope) | car acceleration |
        
        
        $$
        a=\frac{\Delta v}{\Delta t}=\frac{12\frac ms-(-4\frac ms)}{4s-0s}=4m/s^2\\
        x_f=x_i+v_{x_i}\Delta t+\frac12a_x(\Delta t)^2\\
        x=10m+(12\frac ms)(4s-0s)+\frac12(4\frac m{s^2})(4s-0s)^2\\
        x=10m+(12\frac ms)(4s)+\frac12(4\frac m{s^2})((4s)^2)\\
        x=10m+(48\frac {m\cancel s}{\cancel s})+\frac12(4\frac m{s^2})(16s^2)\\
        x=10m+48m+\frac{4*16}{2}\frac {m\cancel{s^2}}{\cancel{s^2}}\\
        x=10m+48m+{2*16}m\\
        x=10m+48m+32m\\
        x=90m\\
        \overline{\underline{|x=9\times 10^1m|}}
        $$
    
2. Does this car ever change direction, if so, at what time?
        The car changes direction at $t=3s$
    
3. When you sneeze, it is claimed that the air droplets in your lungs accelerate from rest to 150km/hr in 0.500s. Assume the droplets accelerate uniformly.

    1. What is the average acceleration of a droplet in $m/s^2$?
       
        | Variable     | Value                       | Significance                                  |
        | ------------ | --------------------------- | --------------------------------------------- |
        | $v^2_{x_f1}$ | $\approx14m/s$ (calculated) | Velocity upon entering the water              |
        | $v^2_{x_f2}$ | $0m/s$ (given)              | Velocity upon touching the bottom of the pool |
        | $v^2_{x_i1}$ | $0m/s$ (given)              | Initial velocity (from the platform)          |
        
        $$
        a=\frac{\Delta v}{\Delta t}=\frac{150\frac{km}{hr}-0}{0.500s-0}\\
    \frac{150\cancel{km}}{1\cancel{hr}}*\frac{10^3m}{1\cancel{km}}*\frac{1\cancel{hr}}{3600s}\\
        \frac{150\cancel{*10^3}}{\cancel{3600}3.6}m/s=41.\overline6m/s\\
        a=\frac{41.\overline6m/s}{0.5000s}\\
        a=83.\overline3m/s^2\\
        \overline{\underline{|a=83.3m/s^2|}}
        $$
        
    2. Assuming the acceleration is constant, how far does the droplet travel in 0.500s?
       
        | Variable   | Value                                         | Significance                                  |
        | ---------- | --------------------------------------------- | --------------------------------------------- |
        | $x_f$      | $\approx14m/s$ (calculated)                   | Velocity upon entering the water              |
        | $x_i$      | $0m/s$ (given)                                | Velocity upon touching the bottom of the pool |
        | $v_{x_i}$  | $0m/s$ (given)                                | Initial velocity (from the platform)          |
        | $a_x$      | $83.3m/s^2$ (calculated in previous question) | Acceleration of droplets                      |
        | $\Delta t$ | $(0.500s-0s)$ (given)                         | given time period for travel                  |

    $$
    x_f=x_i+v_{x_i}\Delta t+\frac12a_x(\Delta t)^2\\
    x_f=0m+(0m/s)(0.500s-0s)+\frac12(83.3m/s^2)(0.500s-0s)^2\\
    x_f=\cancel{0m+(0m/s)(0.500s-0s)}+\frac12(83.3m/s^2)(0.500s)^2\\
    x_f=\frac12(83.3m/s^2)(0.500s)^2\\
    x_f=\frac{83.3}{2*(\frac 12^2)}\frac{\frac m{\cancel{s^2}}}{\cancel{s^2}}\\
    x_f=\frac{83.3}{\cancel{2*}(\frac 12\cancel{^2})}m\\
    x_f=\frac{83.3}{\frac12}m=x_f=2*83.3m\\
    x_f=166.6m\\
    \overline{\underline{|x_f=1.66\times10^2m|}}
    $$

4. Divers compete by diving in a 3.00m deep pool from a platform 10m above the water. What is the magnitude of the minimum acceleration in the water needed to keep a diver from hitting the bottom of a pool? Assume the diver drops from rest from the platform and the acceleration in the water is constant.

| Variable     | Value                       | Significance                                             |
| ------------ | --------------------------- | -------------------------------------------------------- |
| $v^2_{x_f1}$ | $\approx14m/s$ (calculated) | Velocity upon entering the water                         |
| $v^2_{x_f2}$ | $0m/s$ (given)              | Velocity upon touching the bottom of the pool            |
| $v^2_{x_i1}$ | $0m/s$ (given)              | Initial velocity (from the platform)                     |
| $v^2_{x_i2}$ | $v^2_{x_f1}$                | Velocity upon entering the water                         |
| $a_{x1}$     | $-9.81m/s^2$ (given)        | Acceleration due to gravity                              |
| $a_{x2}$     | $33m/s^2$ (calculated)      | Acceleration to avoid impact with the bottom of the pool |
| $\Delta x_1$ | $0m-10m$ (given)            | Distance from the platform to the top of the water       |
| $\Delta x_2$ | $0m-3.00m$ (given)          | distance from the top of the water to the bottom of th   |

$$
v^2_{x_f1}=v^2_{x_i1}+2a_{x1}\Delta x_1\\
v^2_{x_f1}=\cancel{(0m)^2}+2(-9.81m/s^2)(\cancel{0m}-(10m))\\
v^2_{x_f1}=((9.81m/s^2)(20m))\\
\sqrt{v^2_{x_f1}}=\sqrt{196.2m^2/s^2}\\
v_{x_f1}=14.007141m/s\\
\rule{150pt}{0.4pt}\\
v^2_{x_f2}=v^2_{x_i2}+2a_{x2}\Delta x_2\\
0=(14.007141m/s)^2+2a_{x2}(0m-(-3.00m))\\
(-14.007141m/s)^2=2a_{x2}(3.00m)\\
\frac{(-14.007141m/s)^2}{2*3.00m}=a_{x2}\\
\frac{(-14.007141)^2}{6}\frac{\frac {m\cancel{^2}}{s^2}}{\cancel m}=a_{x2}\\
\frac{-196.2}{6}m/s^2=a_{x2}\\
-32.7m/s^2=a_{x2}\\
\overline{\underline{|3.3\times10^1m/s^2=a_{x2}|}}
$$



# In Class Questions

​	1. Why are you in this class?

I am in this class because it is required for my major, and I REALLY didn't want to take calc-based physics because I heard it was extremely difficult.

2. What is the general process for an approach of a physics problem?
    1. Read the question
        1. Understand (or find) the units. 
        2. Find the requested answer, and its associated units.
    2. Use provided equations. (These will be taught in class.)
    3. Convert units (may be optional)
    4. Ensure the answer makes sense.
    5. Reduce to required number of significant figures.
    6. Change answer to scientific notation (or metric prefixes, if allowed). (May be optional, depending on length of value)