# Week 4 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 9/20/2020

# Forces and Newton's Second Law - Part 1

For each of the situations described below, before answering any questions:

- Draw a simple schematic of the physical situation
- identify the object of interest
- identify the forces acting on the object
- draw a free body diagram
- Apply Newton's Second Law in component form

1. A ball of mass $m$ is tied to a string fixed to the ceiling. The ball is pulled to one side and held at rest by an unknown horizontal force $\overrightarrow F$ while the string makes angle $\theta$ with respect to the horizontal (negative from the $0^\circ$ standard)[^2]. 

    ![free body diagram 1](macdougall_skyler_week4.assets/question1FreeBodyDiagram.png)

    1. What is the magnitude of the tension T on the string?[^1] 
        $$
        \sum F=0=\overrightarrow W+\overrightarrow F-\overrightarrow T\\
        \overrightarrow W+\overrightarrow F=\overrightarrow T\\
        gm\ang-90^\circ+|\overrightarrow F|\ang0^\circ=\overrightarrow T\\
        |\overrightarrow T|=\sqrt{{(gm)}^2+{\overrightarrow F}^2}
        $$

    2. What is the magnitude of the Force holding the ball in position?[^1] 
        $$
        |\overrightarrow F|=cos(\theta)
        $$
        Determine the numerical values for T and F if:
        $$
        m=2.00kg\\
        \theta=30.0^\circ
        $$

        $$
        |\overrightarrow F|=cos(\theta);\ |\overrightarrow T|=\sqrt{{(gm)}^2+{\overrightarrow F}^2}\\
        |\overrightarrow F|=cos(30.0^\circ)*1N={\sqrt3\over2}N\approx0.866N\\
        |\overrightarrow T|=\sqrt{{(gm)}^2+{\overrightarrow F}^2}\\
        |\overrightarrow T|=\sqrt{{((9.81m/s^2)(2.00kg))}^2+{(\frac{\sqrt3}2N)}^2}\\
        |\overrightarrow T|=\sqrt{(19.6133N)^2+{\frac34N^2}}\\
        |\overrightarrow T|=19.632N\\
        \underline{\overline{||\overrightarrow F|=0.866N;\ |\overrightarrow T|=19.6N|}}
        $$

2. A student wants to hang an RIT sign with weight $W$ at rest from 2 cables that make angles $\theta_1$ and $\theta_2$ with respect to the horizontal. (Note that $\theta_1$ is negative from the $0^\circ$ standard, while $\theta_2$ is positive from the $180^\circ$ standard) [^2]Let $T_1$ be the unknown tension in cable 1 and $T_2$ the tension in cable 2. Treat the sign as a standard point mass.

    1. Draw a free body diagram for the sign. 

        ![free body diagram 2](macdougall_skyler_week4.assets/question2FreeBodyDiagram.png)

    2. Determine the expression for the tension $T_1$. [^1]
        $$
        \overrightarrow W=\overrightarrow {T_1}+\overrightarrow {T_2}\\
        \overrightarrow{T_{1x}} =\overrightarrow{T_1}\cos(\theta_1);\ \overrightarrow{T_{1y}} =\overrightarrow{T_1}\sin(\theta_1)\\
        \overrightarrow{T_{2x}} =\overrightarrow{T_2}\cos(\theta_2);\ \overrightarrow{T_{2y}} =\overrightarrow{T_2}\sin(\theta_2)\\
        \overrightarrow {W_x}=\overrightarrow {T_{1x}}+\overrightarrow {T_{2x}}\\
        \overrightarrow {W_y}=\overrightarrow {T_{1y}}+\overrightarrow {T_{2y}}\\
        0=\overrightarrow {T_{1x}}+\overrightarrow {T_{2x}}\\\rule{100pt}{0.4pt}\\
        -\overrightarrow {T_{1x}}=\overrightarrow {T_{2x}}\\
        \overrightarrow {W}=\overrightarrow {T_{1y}}+\overrightarrow {T_{2y}}\\
        \overrightarrow {W}-\overrightarrow {T_{1y}}=\overrightarrow {T_{2y}}\\
        \overrightarrow{T_1}=\overrightarrow{T_2}\frac{\cos(\theta_2)}{\cos(\theta_1)}\\
        \overrightarrow{T_2}=\frac{\overrightarrow{W}-\overrightarrow {T_1}\sin(\theta_1)}{\sin(\theta_2)}\\
        \overrightarrow{T_1}=\frac{\overrightarrow{W}-\overrightarrow {T_1}\sin(\theta_1)}{\sin(\theta_2)}\frac{\cos(\theta_2)}{\cos(\theta_1)}\\
        \overrightarrow{T_1}=\frac{\overrightarrow{W}-\overrightarrow {T_1}\tan(\theta_1)}{\tan(\theta_2)}\\
    \overrightarrow{T_1}\tan(\theta_2)=\overrightarrow W-\overrightarrow {T_1}\tan(\theta_1)\\
        \overrightarrow{T_1}(\tan(\theta_2)+\tan(\theta_1))=\overrightarrow W\\
        \overrightarrow{T_1}=\frac{\overrightarrow W}{\tan(\theta_1)+\tan(\theta_2)}\\
        \overline{\underline{|\overrightarrow{T_1}=\frac{\overrightarrow W}{\tan(\theta_1)+\tan(\theta_2)}|}}
        $$
        
    3. Determine the expression for the tension $T_2$. [^1]
        $$
        \overrightarrow{T_2}=\frac{\overrightarrow{W}-\overrightarrow {T_1}\sin(\theta_1)}{\sin(\theta_2)}\\
        \overrightarrow{T_1}=\overrightarrow{T_2}\frac{\cos(\theta_2)}{\cos(\theta_1)}\\
    \overrightarrow{T_2}=\frac{\overrightarrow{W}-\overrightarrow{T_2}\frac{\cos(\theta_2)}{\cos(\theta_1)}\sin(\theta_1)}{\sin(\theta_2)}\\
        \overrightarrow{T_2}\sin(\theta_2)={\overrightarrow{W}-\overrightarrow{T_2}\frac{\cos(\theta_2)}{\cos(\theta_1)}\sin(\theta_1)}\\
        \overrightarrow{T_2}=\frac{\overrightarrow W}{\sin(\theta_2)+\frac{\cos(\theta_2)}{\cos(\theta_1)}\sin(\theta_1)}\\
        \overline{\underline{|\overrightarrow{T_2}=\frac{\overrightarrow W}{\sin(\theta_2)+\tan(\theta_1)\cos(\theta_2)}|}}
        $$
        
    4. The weight of the sign is $W=225N$ and the angles are $\theta_1=25.0^\circ;\ \theta_2=55.0^\circ$. If the student chooses cables that are each capable of supporting a maximum tension of $150N$, will the design work?
        $$
        \overrightarrow{T_1}=\frac{\overrightarrow W}{\tan(\theta_1)+\tan(\theta_2)};\ \overrightarrow{T_2}=\frac{\overrightarrow W}{\sin(\theta_2)+\tan(\theta_1)\cos(\theta_2)}\\
        \overrightarrow{T_1}=\frac{225N}{\tan(25.0^\circ)+\tan(55.0^\circ)}\\
        \overrightarrow{T_1}=\frac{225N}{1.89}\\
        \overrightarrow {T_1}=118.767N\\
        \overrightarrow{T_2}=\frac{\overrightarrow W}{\sin(\theta_2)+\tan(\theta_1)\cos(\theta_2)}\\
        \overrightarrow{T_2}=\frac{225N}{\sin(55.0^\circ)+\tan(25.0^\circ)\cos(55.0^\circ)}\\
        \overrightarrow {T_2}=207.065N\\
        \overline{\underline{|\overrightarrow {T_1}=119N;\ \overrightarrow {T_2}=207N|}}
        $$

    This design will not work.

3. A block of weight $W$ is at rest on a rough surface inclined at an angle $\theta$ with respect to the horizontal (positive from the $0^\circ$ standard)[^2]. 

    1. What is the direction of friction force acting on the block? What direction is the normal force acting on the block? 
        The friction force is in the positive X. (Up the slope of the sloped surface.) The normal force is acting in the positive Y. (Away from the sloped surface.) 

    2. Draw a free body diagram.

        ![free body diagram 3](macdougall_skyler_week4.assets/question3FreeBodyDiagram.png)

    3. Determine the expression for the magnitude of the friction force acting on the block. [^1]
        $$
        \overrightarrow {F_F}=|\overrightarrow{F_F}|\ang0^\circ\\
        \overrightarrow {F_N}=|\overrightarrow {F_N}|\ang90^\circ\\
        \overrightarrow W=|\overrightarrow W|\ang(270-\theta)^\circ\\
        \overrightarrow {F_F}=|\overrightarrow W|\cos(\theta)
        $$
        
    4. Determine the expression for the magnitude of the normal force acting on the block. [^1]
        $$
        \overrightarrow {F_N}=|\overrightarrow W|\sin(\theta)
        $$
        
    5. Determine the numerical values for the magnitude of the frictional force and the normal force if
    
    $$
        W=25.0N\\
        \theta=30.0^\circ
    $$
    
    $$
    \overrightarrow {F_F}=|\overrightarrow W|\cos(\theta);\ \overrightarrow {F_N}=|\overrightarrow W|\sin(\theta)\\
    \overrightarrow {F_F}=25.0N*\cos(30.0^\circ);\ \overrightarrow {F_N}=25.0N\sin(30.0^\circ)\\
    \overline{\underline{|\overrightarrow{F_F}=21.7N;\ \overrightarrow{F_N}=12.5N|}}
    $$
    
    
    
4. A $25.0N$ book is held at rest against a rough vertical surface by force $\overrightarrow{F_{push}}$ of magnitude $18.0N$, directed at $60.0^\circ$ (positive relative to $0^\circ$ standard)[^2]. 

    1. Draw a free body diagram for the book.

        ![free body diagram 4](macdougall_skyler_week4.assets/question4FreeBodyDiagram.png)
        
    2. What is the magnitude and direction of the friction force acting on the book?
        $$
        |\overrightarrow{F_F}|=|\overrightarrow{F_{push}}|sin(\theta)-\overrightarrow W\\
        |\overrightarrow{F_F}|=18.0Nsin(60^\circ)-25.0N\\
        |\overrightarrow{F_F}|=-9.41N\\
        \ang\overrightarrow{F_F}=-90^\circ\\
        \therefore\\
        \overline{\underline{|\overrightarrow{F_F}=9.41N\ang90^\circ|}}
        $$
        Note: $90^\circ$ in this instance refers to directly up on the diagram shown above.

    3. If the magnitude of the push force $\overrightarrow{F_{push}}$ is instead $36.0N$, what is the magnitude and direction of the friction force?
        $$
        |\overrightarrow{F_F}|=|\overrightarrow{F_{push}}|sin(\theta)-\overrightarrow W\\
        |\overrightarrow{F_F}|=36.0Nsin(60^\circ)-25.0N\\
        |\overrightarrow{F_F}|=6.1769N\\
        \ang\overrightarrow{F_F}=-90^\circ\\
        \therefore\\
        \overline{\underline{|\overrightarrow{F_F}=6.18N\ang-90^\circ|}}
        $$
        

5. You are standing on a bathroom scale inside an elevator. Your weight is $140lb$, but the reading of the scale is $120lb$. The scale reads the magnitude of the normal force. ($1lb=4.448N$).

    ![image-20200917144225737](macdougall_skyler_week4.assets/image-20200917144225737.png)

    1. What is the magnitude and direction of the acceleration of the elevator?
        Your weight decreases, meaning the normal force decreases. Therefore, the direction of the acceleration is up.
        $$
        |\overrightarrow {F_a}|=\overrightarrow W-\overrightarrow {F_{normal}}\\
        |\overrightarrow {F_a}|=(140\cancel{lb}*4.448N/\cancel{lb})-(120\cancel{lb}*4.448N/\cancel{lb})\\
        |\overrightarrow {F_a}|=88.96N\\
        \overrightarrow {F_a}=ma\\
        \overrightarrow {F_a}=(\frac{\overrightarrow W}{g})a\\
        (88.95\cancel{N})(\frac{-9.81m/s^2}{622.72\cancel{N}})=a\\
    (88.95)(\frac{-9.81}{622.72})m/s^2=a\\
        a=-1.401m/s^2\\
        \underline{\overline{|a=-1.4m/s^2|}}
        $$
        
    2. Can you tell whether the elevator is speeding up or slowing down? Explain.
        There is no way to know, with the current information available, if the elevator is speeding up or slowing down. It could be speeding up, which would mean its going up. Alternatively, it could be slowing down, which would mean its going down.

6. A $425g$ shuffleboard disk is given an initial speed of $3.20m/s$ across a horizontal court. The disk travels $6.00m$ in a straight line before coming to rest. Assume the acceleration of the disk is constant.

    1. Draw a free body diagram. 

        ![image-20200917000648342](macdougall_skyler_week4.assets/image-20200917000648342.png)

    2. What is the net force exerted on the disk while it is coming to rest?
        "Assume the acceleration of the disk is constant."
        Because acceleration is not zero, then the sum of forces is not zero. The normal force and the weight force will cancel out though, leaving only the drag or kinetic friction force.
        Therefore, the net force is the kinetic friction force, noted for the rest of the problem as $\overrightarrow {F_{drag}}$.

    3. What is the magnitude of the kinetic friction force exerted on the disk by the court?
        $$
        v_f^2=v_i^2+2a\Delta x\\
        v_f^2-v_i^2=2a\Delta x\\
        \frac{v_f^2-v_i^2}{2\Delta x}=a\\
        \frac{(0m/s)^2-(3.20m/s)^2}{2(6.00m)}=a\\
        a=0.85\overline3m/s^2\\
        \overrightarrow {F_{drag}}=ma\\
        \overrightarrow {F_{drag}}=(0.425kg)(0.85\overline3m/s^2)\\
        \overline{\underline{| \overrightarrow {F_{drag}}=363.mN |}}
        $$
    
4. What is the coefficient of kinetic friction between the disk and the court?
        $$
        \mu_k=\frac{\overrightarrow {F_k}}{\overrightarrow {F_N}}\\
        \overrightarrow {F_N}=ma=0.425kg*9.81m/s^2=4.169N\\
        \overrightarrow {F_k}=\overrightarrow {F_{drag}}=363.mN\\
        \mu_k=\frac{363.mN}{4.169N}\\
        \mu_k=0.086986\\
        \overline{\underline{| \mu_k=8.70*10^{-2}|}}
        $$
        
    
7. A box of mass $m$ is released from rest on a smooth ramp inclined at an angle $\theta$ above the horizontal (negative relative to $180^\circ$ standard). Orient the positive x-axis down the ramp.

    1. What does the term "smooth ramp" mean?
        "Smooth ramp" means that friction is negligible.
        
    2. Draw a free body diagram.
    
        ![image-20200917115108763](macdougall_skyler_week4.assets/image-20200917115108763.png)
    
    3. Determine an expression for the acceleration of the box. [^1]Does the acceleration of the box depend on the mass $m$?
        $$
        F=ma;\ \overrightarrow {F_{net}}=\overrightarrow{F_{normal}}\cos{\theta}\\
        \overrightarrow {F_{normal}}=\frac{\sin(\theta)}{\overrightarrow W}\\
        \overrightarrow{F_{net}}=\frac{\overrightarrow W}{\sin(\theta)}\cos(\theta)\\
        a_{F_{net}}=\frac{{\overrightarrow W}{\sin(\theta)}\cos(\theta)}{m}\\
        \overrightarrow W=gm\\
        a_{F_{net}}=\frac{{gm*}{\sin(\theta)}\cos(\theta)}{m}\\a_{F_{net}}=\frac{{g\cancel{m}*}{\sin(\theta)}\cos(\theta)}{\cancel{m}}\\
        \overline{\underline{|a_{F_{net}}=g\sin(\theta)\cos(\theta)|}}
        $$
        No, the acceleration is not dependent on the mass.
    
    4. If the mass is $10.0kg$ and $\theta=55.0^\circ$, what is the magnitude of the acceleration of the box?
        $$
        a_{F_{net}}=g\sin(\theta)\cos(\theta)\\
        a_{F_{net}}=(9.81m/s^2)\sin(55^\circ)\cos(55^\circ)\\
        a_{F_{net}}=4.60919m/s^2\\
        \overline{\underline{|a_{F_{net}}=4.61m/s^2|}}
        $$
    
    5. How long does it take for the box to reach a speed of $10.0m/s$?
        $$
        t=\frac av\\
        t=\frac{4.61m/s^2}{10m/s}\\
        t=0.461s\\
        x_f=x_i+v_it+\frac12at^2\\
        x_f=\cancel{0m+(0m/s)(0.461s)+}\frac12(4.61m/s^2)(0.461s)^2\\
        x_f=\frac12(4.61m/s^2)(0.461s)^2\\
        x_f=0.489m\\
        \overline{\underline{|t=0.461s;\ x_f=0.489m|}}
        $$
    
    6. How far down the ramp has the box slid when it reaches a speed of $10.0m/s$?
        $$
        \overline{\underline{|x_f=0.489m|}}
        $$
        
    7. Draw a velocity-time graph for values of speed between $0$ and $10.0m/s$.
    
        ![image-20200917114126804](macdougall_skyler_week4.assets/image-20200917114126804.png)
    
8. A horizontal force $\overrightarrow F$ causes a block of mass $m$ to slide up a rough incline surface with an acceleration of magnitude $a$. The surface is inclined at an angle $\theta$ above the horizontal (positive from the $0^\circ$ standard). Orient the positive x-axis up the ramp. 

    1. What does the term "rough surface" mean?
        "Rough surface" means that friction is not negligible.
        
    2. What type of friction (kinetic or static) acts on the block? What is the direction of the frictional force acting on the block?
        Because the block is moving, its kinetic friction. It is in the opposite direction of the movement.
        
    3. Draw a free body diagram.

        ![image-20200917120040907](macdougall_skyler_week4.assets/image-20200917120040907.png)

    4. Determine an equation for the magnitude of the frictional force. [^1]
        $\overrightarrow {F_{normal}}=\overrightarrow Wsin(\theta+90^\circ)$ because $\theta$ is the angle from zero, and the normal force is $90^\circ$ farther around the circle (counterclockwise) than $\theta$. This is because the normal force is alway perpendicular to the surface of interaction.
        $$
        \overrightarrow {F_{net}}=ma\ang\theta;\ a\ne g\\
        \overrightarrow {F_{normal}}=\overrightarrow W\sin(\theta+90^\circ)\\
        ma\cos(\theta)=\overrightarrow F-(\overrightarrow {F_{drag}}cos(\theta)+\overrightarrow {F_{normal}}\cos(\theta))\\
        ma\cos(\theta)+\overrightarrow {F_{drag}}cos(\theta)=\overrightarrow F-\overrightarrow {F_{normal}}\cos(\theta)\\
        \overrightarrow {F_{drag}}\cos(\theta)=\overrightarrow F-\overrightarrow{F_{normal}}\cos(\theta)-ma\cos(\theta)\\
        \overrightarrow {F_{drag}}=\frac{\overrightarrow F}{\cos\theta}-\overrightarrow {F_{normal}}-ma\\
        \overline{\underline{|\overrightarrow {F_{drag}}=\frac{\overrightarrow F}{\cos\theta}-mg\sin(\theta+90^\circ)-ma|}}
        $$

    5. Determine an equation for the magnitude of the normal force. [^1]
        $$
        \overrightarrow {F_{normal}}=\overrightarrow W\sin(\theta)\\
        \underline{\overline{|\overrightarrow {F_{normal}}=mg\sin(\theta)|}}
        $$

    6. If $\overrightarrow F=200N;\ m=15.0kg;\ a=25.0m/s^2;\ \theta=20.0^\circ$, calculate the numerical values for the magnitudes of the normal and friction forces.
        $$
        \overrightarrow {F_{drag}}=\frac{\overrightarrow F}{\cos\theta}-mg\sin(\theta+90^\circ)-ma;\ \overrightarrow {F_{normal}}=mg\sin(\theta)\\
        \overrightarrow {F_{drag}}=\frac{200N}{\cos(20^\circ)}-(15kg)(-9.81m/s^2)\sin(20^\circ+90^\circ)-(15kg)(25m/s^2)\\
        \overrightarrow {F_{drag}}=23.88868N\\
        \overrightarrow {F_{normal}}=(15.0kg*9.81m/s^2)\sin(20.0^\circ)\\
        \overrightarrow {F_{normal}}=50.328N\\
        \overline{\underline{|\overrightarrow {F_{drag}}=23.9N;\ \overrightarrow {F_{normal}}=50.3N|}}
        $$

    7. What is the coefficient of friction between the block and the incline surface?
        $$
        \mu_k=\frac{\overrightarrow {F_k}}{\overrightarrow {F_N}};\ \overrightarrow{F_k}=\overrightarrow{F_{drag}}\\
        \mu_k=\frac{23.9N}{50.3N}\\
        \mu_k=0.474657\\
        \overline{\underline{|\mu_k=0.475|}}
        $$

9. A box of box is initially at rest. At $t=0$, you start pulling with an applied horizontal force $\overrightarrow {F_a}$. The mass of the box is $80.0kg$. The coefficient of static friction between the bottom of the box and the wooden floor is $0.751$, and the coefficient of kinetic friction is $0.691$. 
    For each of the following applied forces, determine the magnitude of the friction force on the box and the corresponding horizontal acceleration of the box.

    ![image-20200917124452286](macdougall_skyler_week4.assets/image-20200917124452286.png)
    
    Static and kinetic friction values are based off of the normal force, which is based off of the weight force, which does not change in this problem. Therefore, the friction forces can be calculated without the given problems.
    $$
    \overrightarrow{F_{static}}=\mu_k\overrightarrow{F_N}\\
    \overrightarrow{F_{static}}=\mu_k(m)(-g)\\
    \overrightarrow{F_{static}}=0.751(80.0kg)(9.81m/s^2)\\
    \overrightarrow{F_{static}}=589.3848N\\\rule{100pt}{0.6pt}\\
    \overrightarrow{F_{kinetic}}=\mu_k\overrightarrow{F_N}\\
    \overrightarrow{F_{kinetic}}=\mu_k(m)(-g)\\
    \overrightarrow{F_{kinetic}}=0.751(80.0kg)(9.81m/s^2)\\
    \overrightarrow{F_{kinetic}}=542.2968N\\\rule{100pt}{0.6pt}\\
    \overline{\underline{|\overrightarrow{F_{static}}=589.3848N;\ \overrightarrow{F_{kinetic}}=542.2968N|}}
    $$
    
    1. Applied force is $242N$
        $$
        \overrightarrow{F_{static_{max}}}>\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{static}}=\overrightarrow{F_{applied}}=242N\\
        \sum\overrightarrow F=0\\\therefore\\
        a=0\\
        \overline{\underline{|\overrightarrow{F_{static}}=242N;\ a=0|}}
        $$
    
    2. $407N$
        $$
        \overrightarrow{F_{static_{max}}}>\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{static}}=\overrightarrow{F_{applied}}=407N\\
        \sum\overrightarrow F=0\\\therefore\\
        a=0\\
        \overline{\underline{|\overrightarrow{F_{static}}=407N;\ a=0|}}
        $$
    
    3. $545N$
        $$
        \overrightarrow{F_{static_{max}}}>\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{static}}=\overrightarrow{F_{applied}}=545N\\
        \sum\overrightarrow F=0\\\therefore\\
        a=0\\
        \overline{\underline{|\overrightarrow{F_{static}}=545N;\ a=0|}}
        $$
    
    4. $559N$
        $$
        \overrightarrow{F_{static_{max}}}>\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{static}}=\overrightarrow{F_{applied}}=559N\\
        \sum\overrightarrow F=0\\\therefore\\
        a=0\\
        \overline{\underline{|\overrightarrow{F_{static}}=559N;\ a=0|}}
        $$
    
    5. $619N$
        $$
        \overrightarrow {F_{static_{max}}}<\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{kinetic}}=542.2968N\\
        \overrightarrow {F_{net}}=\overrightarrow{F_{applied}}-\overrightarrow{F_{kinetic}}\\
        a=\frac{\overrightarrow{F_{net}}}{m}\\
        a=\frac{\overrightarrow{F_{applied}}-\overrightarrow{F_{kinetic}}}{m}\\
        a=\frac{619N-542.2968N}{80kg}\\
        a=0.65879m/s^2\\
        \overline{\underline{|\overrightarrow{F_{kinetic}}=542.N;\ a=0.659m/s^2|}}
        $$
    
    6. $719N$
        $$
        \overrightarrow {F_{static_{max}}}<\overrightarrow{F_{applied}}\\\therefore\\
        \overrightarrow{F_{kinetic}}=542.2968N\\
        \overrightarrow {F_{net}}=\overrightarrow{F_{applied}}-\overrightarrow{F_{kinetic}}\\
        a=\frac{\overrightarrow{F_{net}}}{m}\\
        a=\frac{\overrightarrow{F_{applied}}-\overrightarrow{F_{kinetic}}}{m}\\
        a=\frac{719N-542.2968N}{80kg}\\
        a=2.20879m/s^2\\
        \overline{\underline{|\overrightarrow{F_{kinetic}}=542.N;\ a=2.21m/s^2|}}
        $$
        
    7. For $719N$, how far does the box go in $2.32s$ if the box was initially at rest? 
        $$
        x_f=x_i+v_it+\frac12at^2\\
        x_f=\cancel{x_i+v_it+}^0+\frac12(2.21m/s^2)(2.32s)^2\\
        \overline{\underline{|x_f=5.94m}}|
        $$
        

[^1]: Write the result in terms of given quantities and constants.

[^2]:All angles are taken as absolute, with "$0^\circ$ standard" referring to directly right, and "$180^\circ$ standard" referring to directly left in all diagrams. Furthermore, "positive from" refers counterclockwise movement from the noted standard, while "negative from" refers to clockwise movement from the noted standard.