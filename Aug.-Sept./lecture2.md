1. When jumping, a flea reaches a takeoff speed of $1.00m/s$ over a distance of $0.500mm$.

    ![image-20200831144218815](lecture2.assets/image-20200831144218815.png)

    | Variable          | Value                 | Significance                                            |
    | ----------------- | --------------------- | ------------------------------------------------------- |
    | $v_0=v_{takeoff}$ | $1.00m/s$ (given)     | velocity achieved when transitioning from no $g$ to $g$ |
    | $\Delta y_1$      | $0.500mm-0mm$ (given) | change in displacement without being affected by $g$    |
    | $a_1$             |                       | acceleration due to jump                                |
    | $a_2$             | $9.81m/s^2$ (given)   | acceleration due to gravity                             |
    | $v_1$             | $0m/s$ (given)        | velocity at peak of jump                                |
    | $y_2$             |                       |                                                         |
    |                   |                       |                                                         |
    |                   |                       |                                                         |
    |                   |                       |                                                         |

    

    1. What is the flea's acceleration during the jump phase?
        $$
        v_{takeoff}^2=v_i^2+2a_1\Delta y_1\\
        v_{takeoff}^2=\cancel{v_i^2+}2a_1\Delta y_1\\
        v_{takeoff}^2=2a_1\Delta y_1\\
        v_{takeoff}^2=2a_1\Delta y_1\\
        \frac{v_{takeoff}^2}{2\Delta y_1}=a_1\\
        \frac{(1.00\cancel{m}/s*\frac{10^3mm}{1\cancel{m}})^2}{2(0.500mm-0mm)}=a_1\\
        \frac{(1.00*{10^3})^2}{2(0.500-0)}\frac{\frac{mm^2}{s^2}}{mm}=a_1\\
        \frac{(1.00*{10^3})^2}{2(0.500)}\frac{\frac{mm}{s^2}}{1}=a_1\\
        \frac{(1.00*{10^3})^2}{1}\frac{mm}{s^2}=a_1\\
        {(1.00*{10^3})^2}\frac{mm}{s^2}=a_1\\
        {1.00*{10^6}}\frac{mm}{s^2}=a_1\\
        {1.00*{10^3}}\frac{m}{s^2}=a_1\\
        \underline{\overline{|{1.00*{10^3}}m/s^2=a_1|}}
        $$

    2. How long does the acceleration phase last?
        $$
        a=\frac{\Delta v}{\Delta t}\\
        \Delta t=\frac{\Delta v}{a_1}\\
        \Delta t=\frac{v_{takeoff}-v_i}{a_1}\\
        \Delta t=\frac{1m/s-0}{1\times10^3m/s^2}\\
        \Delta t=\frac{1m/s}{1\times10^3m/s^2}\\
        \Delta t=\frac{1}{1\times10^3}s\\
        \underline{\overline{|\Delta t=1.00\times 10^-3s|}}
        $$

    3. If the flea jumps straight up, how high will it go? Ignore air resistance for ease of math.
        $$
        v_{max}^2=v_{takeoff}^2+2a_2\Delta y_2\\
        v_{max}^2=v_{takeoff}^2+2a_2\Delta y_2\\
        v_{max}^2-v_{takeoff}^2=2a_2\Delta y_2\\
        \frac{v_{max}^2-v_{takeoff}^2}{2a_2}=\Delta y_2\\
        \frac{0-(1.00m/s)^2}{2(-9.81m/s^2)}=\Delta y_2\\
        \frac{(1.00m/s)^2}{2(9.81m/s^2)}=\Delta y_2\\
        \frac{(1.00m/s)^2}{2(9.81m/s^2)}\frac{m^2/\cancel{s^2}}{m/\cancel{s^2}}=\Delta y_2\\
        \frac{(1.00m/s)^2}{2(9.81m/s^2)}m=\Delta y_2\\
        $$