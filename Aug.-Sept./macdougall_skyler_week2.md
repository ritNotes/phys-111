# Week 1 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 8/24/2020

# 1-D Kinematics with a change in acceleration and/or change in direction

1. You're driving down the highway late one night at $20.0m/s$ when a deer steps out onto the road $35.0m$ in front of you. Your reaction time before stepping on the brakes is $0.500s$, and the maximum deceleration of your car is $10.0m/s^2$. ***Numerical calculations*** are required, show your work.

    1. Carefully sketch the **acceleration time, velocity time, and position time graphs** for your car. Numerical values on all axes are required. Show calculations.
        $$
        x=vt\\
        x=(20.0m/s)(0.500s)\\
        x=10m\\
        \rule{100pt}{0.4pt}\\
        v_f=v_i+a(\Delta t)\\
        0m/s=20m/s-10m/s^2(t_f-0.5s)\\
        20m/s=10m/s^2(t_f-0.5s)\\
        \frac{20m/s}{10m/s^2}=(t_f-0.5s)\\
        \frac{20}{10}s=t_f-0.5s\\
        \frac{20}{10}s+0.5s=t_f\\
        2.5s=t_f\\
        \rule{100pt}{0.4pt}\\
        x_f=x_i+v_i(\Delta t)+\frac12a(\Delta t)^2\\
        x_f=10m+20m/s(2.5s-0.5s)+\frac12(-10m/s^2)(2.5s-0.5s)^2\\
        x_f=10m+20m/s(2s)+\frac12(-10m/s^2)(2s)^2\\
        x_f=10m+40m+\frac12(-10m/s^2)(4s^2)\\
        x_f=10m+40m-20m\\
        x_f=30m
        $$
        ![image-20200831190438503](macdougall_skyler_week2.assets/image-20200831190438503.png)

    2. Do you hit the deer? Why or why not?
        No. When your speed is zero, your distance from where you were when the deer stepped out into the street is 30m, while the deer is 35m away when it steps out onto the street. This means that there is 5m between you and the deer when you come to a whiplash-inducing halt.

2. A hot-air balloon is **ascending** at a rate of $2.00m/s$ when a passenger drops a camera over the side. The camera is $45.0m$ above the ground when it is dropped.

    1. Choose a positive direction for your positive axis, and make a simple drawing of the camera's motion from the moment it leaves the balloon. Use vector arrows in your drawing to show the directions of the camera's initial and final velocity. You can also use a double-wide arrow to show the direction of the acceleration. 

        ![image-20200901152126535](macdougall_skyler_week2.assets/image-20200901152126535.png)

    2. What is the velocity of the camera just before it hits the ground?
        $$
        v_f^2=v_i^2+2a(x_f-x_i)\\
        v_f^2=(2.00m/s)^2+2(-9.81m/s^2)(0m-45.0m)\\
        v_f=\sqrt{2.00m/s + 2(-9.81m/s^2)(-45.0m)}\\
        v_f=\sqrt{2.00m/s+(9.81m/s^2)(90.0m)}\\
        v_f=\pm29.7m/s\\
        \overline{\underline{|V_f=-29.7m/s|}}
        $$

    3. How long does it take for the camera to reach the ground?
        $$
        v_f=v_i+a_x(t_f-t_i)\\
        v_f-v_i=a_x(t_f-t_i)\\
        \frac{v_f-v_i}{a_x}=t_f-t_i\\
        \frac{v_f-v_i}{a_x}+t_i=t_f\\
        t_i+\frac{v_f-v_i}{a_x}=t_f\\
        0s+\frac{-29.7m/s-2.00m/s}{-9.81m/s}=t_f\\
        \frac{-31.7m/s}{-9.81m/s^2}=t_f\\
        \underline{\overline{|t_f=3.23s|}}
        $$
    
4. Carefully sketch the **acceleration time, velocity time, and position time graphs** for the camera. Numerical values on all axes are required. Show calculations.
        ![image-20200901151825285](macdougall_skyler_week2.assets/image-20200901151825285.png)
    
5. Suppose the balloon is **decending** at a rate of $2.00m/s$ when the camera is dropped from a height of $45.0m$ above the ground. What is its speed the instant it hits the ground, and how long does it take to hit the ground?
        $$
        v_f^2=v_i^2+2a(x_f-x_i)\\
        v_f^2=(-2.00m/s)^2+2(-9.81m/s^2)(0m-45.0m)\\
        v_f=\sqrt{-2.00m/s + 2(-9.81m/s^2)(-45.0m)}\\
        v_f=\sqrt{-2.00m/s+(9.81m/s^2)(90.0m)}\\
        v_f=\pm29.7m/s\\
        \overline{\underline{|V_f=-29.7m/s|}}
        $$
    
    $$
        v_f=v_i+a_x(t_f-t_i)\\
        v_f-v_i=a_x(t_f-t_i)\\
        \frac{v_f-v_i}{a_x}=t_f-t_i\\
        \frac{v_f-v_i}{a_x}+t_i=t_f\\
        t_i+\frac{v_f-v_i}{a_x}=t_f\\
        0s+\frac{-29.7m/s-(-2.00m/s)}{-9.81m/s}=t_f\\
        \frac{-27.7m/s}{-9.81m/s^2}=t_f\\
        \underline{\overline{|t_f=2.83s|}}
    $$
    
    
    
3. A rocket, initially at rest on the ground, accelerates straight upward from rest with a constant acceleration of $58.8m/s^2$. The acceleration period lasts for $9.00s$. After that, the rocket is in free fall. 

    1. Find the maximum height $y_{max}$ reached by the rocket.
        $$
        v_f=v_i+a_x(t_f-t_i)\\
        v_f=0m/s+(58.8m/s^2)(9.00s-0.00s)\\
        v_f=(58.8m/s^2)(9.00s)\\
        v_f=529.2m/s\\
        \rule{100pt}{0.4pt}\\
        v_f^2=v_i^2+2a(x_f-x_i)\\
        v_f^2-v_i^2=2a(x_f-x_i)\\
        \frac{v_f^2-v_i^2}{2a}=(x_f-x_i)\\
        \frac{v_f^2-v_i^2}{2a}+x_i=x_f\\
        \frac{(529.2m/s)^2-(0m/s)^2}{2(58.8m/s^2)}+0m=x_f\\
        \frac{(529.2m/s)^2}{2(58.8m/s^2)}=x_f\\
        x_f=2381.4m\\
        \rule{100pt}{0.4pt}\\
        v_f=v_i+a_x(t_f-t_i)\\
        t_i+\frac{v_f-v_i}{a_x}=t_f\\
        9.00s+\frac{0m/s-529.2m/s}{-9.81m/s^2}=t_f\\
        9.00s+\frac{529.2m/s}{9.81m/s^2}=t_f\\
        9.00s+53.94s=t_f\\
        62.94s=t_f\\
        \rule{100pt}{0.4pt}\\
        v_f^2=v_i^2+2a(x_f-x_i)\\
        v_f^2-v_i^2=2a(x_f-x_i)\\
        \frac{v_f^2-v_i^2}{2a}=(x_f-x_i)\\
        \frac{v_f^2-v_i^2}{2a}+x_i=x_f\\
        \frac{(0m)^2-(529.2m/s)^2}{2(-9.81m/s^2)}+2381.4m=x_f\\
        \frac{(529.2m/s)^2}{2(9.81m/s^2)}+2381.4m=x_f\\
        x_f=16655.235m\\
        \overline{\underline{|y_{max}=16.6km|}}
        $$
    
    2. Carefully sketch the rocket's **velocity time graph** for the time interval between first firing and impact with the ground. Label the velocity and time axes with numerical values. 
    
        ![image-20200901164434010](macdougall_skyler_week2.assets/image-20200901164434010.png)



# Vector Algebra - Component Technique

1. An ant undergoes 3 successive displacements:
    $$
    \overrightarrow {d_1}=3.00m,\ 20.0^\circ\ south\ of\ east\\
    \overrightarrow {d_2}=2.00m,\ north\\
    \overrightarrow {d_3}=7.00m,\ 35.0^\circ\ west\ of\ south
    $$

    1. Carefully sketch and label the 3 displacements with their tails at the origin. Use a coordinate system in which the positive x-axis is east and the positive y axis is north.
    
        ![image-20200903142547804](macdougall_skyler_week2.assets/image-20200903142547804.png)
    
    2. Calculate the x and y components of each of the displacements.
        $$
        (d_{1})_x=3.00m(cos(360^\circ-20^\circ));\ east=0^\circ=360^\circ\\
        (d_{1})_x=3.00m(cos(340^\circ))\\
        (d_{1})_x=3.00m(0.939)\\
        (d_{1})_x=2.82m\\
        \rule{100pt}{0.4pt}\\
        (d_{1})_y=3.00m(sin(360^\circ-20^\circ));\ east=0^\circ=360^\circ\\
        (d_{1})_y=3.00m(sin(340^\circ))\\
        (d_{1})_y=3.00m(-0.342)\\
        (d_{1})_y=-1.02m\\
        \rule{100pt}{0.4pt}\\
        (d_{2})_x=2.00m(cos(90^\circ));\ east=0^\circ=360^\circ\\
        (d_{2})_x=2.00m(cos(90^\circ))\\
        (d_{2})_x=2.00m(0)\\
        (d_{2})_x=0.00m\\
        \rule{100pt}{0.4pt}\\
        (d_{2})_y=2.00m(sin(90^\circ));\ east=0^\circ=360^\circ\\
        (d_{2})_y=2.00m(sin(90^\circ))\\
        (d_{2})_y=2.00m(1)\\
        (d_{2})_y=2.00m\\
        \rule{100pt}{0.4pt}\\
        (d_{3})_x=3.00m(cos(270^\circ-35^\circ));\ east=0^\circ=360^\circ\\
        (d_{3})_x=7.00m(cos(235^\circ))\\
        (d_{3})_x=7.00m(-0.574)\\
        (d_{3})_x=-4.02m\\
        \rule{100pt}{0.4pt}\\
        (d_{3})_y=3.00m(sin(270^\circ-35^\circ));\ east=0^\circ=360^\circ\\
        (d_{3})_y=7.00m(sin(235^\circ))\\
        (d_{3})_y=7.00m(-0.819)\\
        (d_{3})_y=-5.73m
        $$
    
        | Variable | X component | Y Component |
        | -------- | ----------- | ----------- |
        | $d_1$    | $2.82m$     | $-1.02m$    |
        | $d_2$    | $0.00m$     | $2.00m$     |
        | $d_3$    | $-4.02m$    | $-5.73m$    |
    
    3. Use the results in part 2 to calculate the distance between where it begins and ends.
        $$
        d_x=\sum (d_n)_x\\
        d_x=(2.82m+0.00m-4.02m)\\
        d_x=-1.20m\\
        \rule{100pt}{0.4pt}\\
        d_y=\sum (d_n)_y\\
        d_y=(-1.02m+2.00m-5.73m)\\
        d_y=-4.76m\\
        \rule{100pt}{0.4pt}\\
        \theta_x=tan^{-1}(\frac{d_x}{d_y})\\
        \theta_x=tan^{-1}(\frac{-1.20m}{-4.76m})\\
        \theta_x=-104^\circ\\
        \rule{100pt}{0.4pt}\\
        |d_{total}|=\sqrt{d_x^2+d_y^2}\\
        |d_{total}|=\sqrt{(-1.20m)^2+(-4.76m)^2}\\
        |d_{total}|=4.91m\\
        \overline{\underline{|d_{total}=4.91m\ang-104.^\circ|}}\\
        \overline{\underline{|d_{total}=4.91m\ang14.1^\circ\ west\ of\ south|}}\\
        \overline{\underline{|d_{total}=4.91m\ang256.^\circ|}}\\
        $$
    
    4. What displacement is necessary to return the ant to its initial position?
        $$
        -4.91m\ang-104.^\circ\\
        4.91m\ang75.9^\circ
        $$
        
    
2. 



# 2-D Motion/Projectile Motion - Part 1

1. In 1780, in what is now referred to as "Brady's Leap", Captain Sam Brady of the US Continental Army escaped certain death from his enemies by running the edge of the cliff above Ohio's Cuyahoga River, which is confined at that spot to a gorge. He landed safely on the far side of the river. 

    ![image-20200903110954934](macdougall_skyler_week2.assets/image-20200903110954934.png)

    1. Representing the horizontal distance jumped as L and the vertical drop as h, as shown in the diagram, derive an expression for the minimum horizontal speed ($v_i$) he would need if he made his leap straight off the cliff.
        $$
        L=\overrightarrow {v_t}*t\\
        h=\frac12gt^2\\
        \frac{L}{\overrightarrow{v_t}}=t;\ t=\sqrt{\frac{2h}{g}}\\
        \frac{L}{\overrightarrow{v_t}}=\sqrt{\frac{2h}{g}}\\
        \frac{L}{1}=\overrightarrow{v_t}\sqrt{\frac{2h}{g}}\\
        \frac{L}{\sqrt{\frac{2h}{g}}}=\overrightarrow{v_t}\\
        $$
        
    2. Evaluate your expression, given the following. Calculate in $m/s$.
        $$
        L=22ft\\
        h=20ft\\
        1m=3.28ft
        $$
        
        $$
        \frac{22\cancel{ft}}{1}\times\frac{1m}{3.28\cancel{ft}}\approx6.707m\\
        \frac{20\cancel{ft}}{1}\times\frac{1m}{3.28\cancel{ft}}\approx6.098m\\
        \frac{L}{\sqrt{\frac{2h}{g}}}=\overrightarrow{v_t}\\
        \frac{6.707m}{\sqrt{\frac{2(6.098m)}{9.81m/s^2}}}=\overrightarrow{v_t}\\
        \frac{6.707m}{\sqrt{\frac{2(6.098)}{9.81}\frac m{m/s^2}}}=\overrightarrow{v_t}\\
        \frac{6.707m}{\sqrt{\frac{12.195}{9.81}\frac 1{1/s^2}}}=\overrightarrow{v_t}\\
        \frac{6.707m}{\sqrt{\frac{12.195}{9.81}s^2}}=\overrightarrow{v_t}\\
        \frac{6.707m}{\sqrt{\frac{12.195}{9.81}}s}=\overrightarrow{v_t}\\
        \frac{6.707}{\sqrt{1.243}}m/s=\overrightarrow{v_t}\\
        \frac{6.707}{1.114}m/s=\overrightarrow{v_t}\\
        6.0158m/s=\overrightarrow{v_t}\\
        \overline{\underline{|6.0m/s=\overrightarrow{v_t}|}}
        $$
    
2. Is it reasonable that a person could make this leap? Use the fact that the world record for the 100m dash is approximately $10s$ to estimate the maximum speed such a runner would have.
    $$
    v_{max}=\frac dt\\
    v_{max}=\frac {100m}{10s}\\
    v_{max}=\frac {100}{10}m/s\\
    v_{max}=10m/s\\
    \overrightarrow{v_t}<v_{max}\\
    \therefore\\
    It\ is\ reasonable\ for\\ a\ person\ to\ make\\ this\ leap.
    $$

3. In a shot-put event, an athlete throws the shot with an initial velocity of $\overrightarrow {v_i}=12.0m/s \ang40.0^\circ$. The shot leaves her hand at a height of $1.80m$ above the ground.
    $$
    \overrightarrow{v_i}=12.0m/s\ang40^\circ\\
    v_{i_x}=12m/s(cos(40^\circ))\approx9.192m/s\\
    v_{i_y}=12m/s(sin(40^\circ))\approx7.713m/s
    $$

    1. How long is the shot in the air?
        $$
        v_{f_y}^2=v_{i_y}^2+2a_y(x_{f_y}-x_{i_y})\\
        v_{f_y}=-\sqrt{v_{i_y}^2+2a_y(x_{f_y}-x_{i_y})}\\
        v_{f_y}=v_{i_y}+a_y(t_f-t_i)\\
        v_{f_y}-v_{i_y}=a_y(t_f-t_i)\\
        \frac{v_{f_y}-v_{i_y}}{a_y}=(t_f-t_i)\\
        \frac{v_{f_y}-v_{i_y}}{a_y}+t_i=t_f\\
        \frac{-\sqrt{v_{i_y}^2+2a_y(x_{f_y}-x_{i_y})}-v_{i_y}}{a_y}+t_i=t_f\\
        \frac{-\sqrt{(7.713m/s)^2+2(-9.81m/s^2)(0m-1.8m)}-7.713m/s}{-9.81m/s^2}+0s=t_f\\
        \frac{-\sqrt{((7.713)^2+2(-9.81)(-1.8))m^2/s^2}-7.713m/s}{-9.81m/s^2}=t_f\\
        \frac{(-\sqrt{(7.713)^2+2(-9.81)(-1.8)})m/s-7.713m/s}{-9.81m/s^2}=t_f\\
        \frac{(-\sqrt{(7.713)^2+2(-9.81)(-1.8)})m/s-7.713m/s}{-9.81m/s^2}=t_f\\
        \frac{-\sqrt{(7.713)^2+2(-9.81)(-1.8)}-7.713}{-9.81}\frac{m/s}{m/s^2}=t_f\\
        \frac{-\sqrt{(7.713)^2+2(-9.81)(-1.8)}-7.713}{-9.81}\frac{1}{1/s}=t_f\\
        \frac{-\sqrt{(7.713)^2+2(-9.81)(-1.8)}-7.713}{-9.81}s=t_f\\
        \frac{-\sqrt{(7.713)^2+35.316}-7.713}{-9.81}s=t_f\\
        \frac{-9.737-7.713}{-9.81}s=t_f\\
        \frac{-17.451}{-9.81}s=t_f\\
        1.7789s=t_f\\
        \overline{\underline{|1.78s=t_f|}}
        $$

    2. How far horizontally does the shot travel?
        $$
        d_x=\frac{v_x}{\Delta t}\\
        d_x=\frac{9.912m/s}{1.78s-0s}\\
        d_x=\frac{9.912m/s}{1.78s}\\
        d_x=\frac{9.912}{1.78}\frac{m/s}{s}\\
        d_x=\frac{9.912}{1.78}m\\
        d_x=5.1676m\\
        \underline{\overline{|d_x=5.17m|}}
        $$

    3. What is the maximum height of the shot above the ground?
        $$
        d_{y_{max}}\ when\ v_y=0m/s\\
        v_{f_y}^2=v_{i_y}^2+2a_y(x_{f_y}-x_{i_y})\\
        v_{f_y}^2-v_{i_y}^2=2a_y(x_{f_y}-x_{i_y})\\
        \frac{v_{f_y}^2-v_{i_y}^2}{2a_y}=x_{f_y}-x_{i_y}\\
        \frac{v_{f_y}^2-v_{i_y}^2}{2a_y}+x_{i_y}=x_{f_y}\\
        \frac{0m/s^2-(7.713m/s)^2}{2(-9.81m/s^2)}+1.80m=x_{f_y}\\
        \frac{-(7.713m/s)^2}{2(-9.81m/s^2)}+1.80m=x_{f_y}\\
        \frac{-(7.713)^2m^2/s^2}{2(-9.81)m/s^2}+1.80m=x_{f_y}\\
        \frac{-(7.713)^2}{2(-9.81)}\frac{m^2/s^2}{m/s^2}+1.80m=x_{f_y}\\
        \frac{-(7.713)^2}{-19.62}\frac{m}{1}+1.80m=x_{f_y}\\
        \frac{-59.497}{-19.62}m+1.80m=x_{f_y}\\
        (\frac{59.497}{19.62}+1.80)m=x_{f_y}\\
        (3.03248+1.80)m=x_{f_y}\\
        4.832m=x_{f_y}\\
        \overline{\underline{|4.83m=x_{f_y}|}}
        $$

    4. How long does it take for the shot to reach its maximum height?
        $$
        v_{f_y}=v_{i_y}+a_y(t_f-t_i)\\
        v_{f_y}-v_{i_y}=a_y(t_f-t_i)\\
        \frac{v_{f_y}-v_{i_y}}{a_y}=(t_f-t_i)\\
        \frac{v_{f_y}-v_{i_y}}{a_y}+t_i=t_f\\
        \frac{0m/s-7.713m/s}{-9.81m/s^2}+0s=t_f\\
        \frac{-7.713m/s}{-9.81m/s^2}=t_f\\
        0.786s=t_f\\
        \overline{\underline{|0.786s=t_f|}}
        $$

    5. Carefully sketch the horizontal and vertical components of the shot's velocity as a function of time. Label all axes with numerical values.

        ![image-20200903122958061](macdougall_skyler_week2.assets/image-20200903122958061.png)

    6. Carefully sketch the horizontal and vertical components of the shot's position as a function of time. Label all axes with numerical values.

        ![image-20200903121958162](macdougall_skyler_week2.assets/image-20200903121958162.png)