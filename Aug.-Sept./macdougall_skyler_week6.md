# Week 6 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 9/25/2020

# Forces And Newton's Second Law

1. A helicopter is lifting two crates simultaneously. Crate A with mass $m_A$, is attached to the helicopter by a light cable. Crate B, with a mass of $m_B$, hangs below crate A and is attached to crate A by a second light cable. The cables are not connected to each other. The helicopter accelerates upward with an acceleration of magnitude $a$. Ignosre all frictional effects. Let $\overrightarrow {T_1}$ be the tension in cable 1, and $\overrightarrow {T_2}$ the tension in cable 2.

    ![image-20201003165056892](macdougall_skyler_week6.assets/image-20201003165056892.png)

    1. Which cable has greater tension? Don't do any math. Explain why.
        Cable 1 will have more tension than cable 2. Cable 1 has to hold up both crates, while cable 2 only holds one crate.

    2. Find $T_2$. [^1]
        $$
        T_2=m_B(a+g)
        $$
        
3. Find $T_1$. [^1]
        $$
        T_1=(m_A+m_B)(a+g)
        $$
        
    4. Find $T_1$ and $T_2$ given:
    $$
        m_A=200kg\\
        m_B=250kg\\
        a=1.00m/s^2
        $$
    
        $$
    T_2=m_B(a+g);\ T_1=(m_A+m_B)(a+g)\\
        T_2=250kg(1.00m/s^2+(-9.81m/s^2))\\
        T_2=-2.2025kN\\\rule{75pt}{0.4pt}\\
        T_1=(m_A+m_B)(a+g)\\
        T_1=(200kg+250kg)(1.00m/s^2+(-9.81m/s^2))\\
        T_1=-3.9645kN\\
        \overline{\underline{|T_1=-4kN;\ T_2=-2kN|}}
        $$
        
        
        
    5. If the helicopter moves vertically upward with a constant velocity, which cable has greater tension? Why? Calculate the values.
        The tension changes, but equally on each cable, so cable 1 will still have more tension.
    
2. Two blocks are connected by a light cord. The cord goes across a pulley, suspending Block 2. Block 1 is on the horizontal surface above Block 2. Block 1 has a weight $W_1=25.0N$, and block 2 has a weight $W_2=18.0N$. The pulley and cords are ideal. Block 1 is at rest.

    ![image-20201003165108485](macdougall_skyler_week6.assets/image-20201003165108485.png)

    1. What does rough surface mean?
        There is friction.

    2. Draw a free body diagram.

    3. What is the magnitude of the tension in the cord?
        $$
        T=W_2\\
        T=18.0N
        $$
        
4. What is the magnitude and direction of the friction force on $W_1$?
        $$
        F_{friction}=T\\
        F_{friction}=18.0N
        $$
        The friction force is pointed in the opposite direction of the tension force.
    
3. A Mass $m_1$ is connected by a light rope passing over a pulley to a second mass $m_2$, like before. Assume no friction and ideal components. 

    ![image-20201003165120625](macdougall_skyler_week6.assets/image-20201003165120625.png)

    1. What does the term "light rope" mean?
        The rope does not have to be accounted for.

    2. Find the magnitude of the acceleration of each mass. [^1]
        $$
        a_2=g\\
        F=ma;\ a_1m_1=m_2a_2\\
        \overline{\underline{|a_1=\frac{m_2g}{m_1};\ a_2=g|}}
        $$
        
3. Find the tension on the rope.[^1]
        $$
        T=-m_2g
        $$
        
    4. Check if your results are reasonable by evaluating numerical values for acceleration and tension at these limits:
    1. What are the values for a and T as $m_1$ approaches 0? Does this make sense?
            Tension and $a_2$ do not change as $m_1$ changes. $a_1$ increases as $m_1$ decreases. All of these make logical sense because it takes less effort to move a lighter object.
        2. What are the values for a and T as $m_2$ approaches 0? Does this make sense?
            $a_2$ does not change. This makes sense, because acceleration due to gravity is mass-independent. Tension and $a_1$ do change, both decreasing as $m_2$ decreases. This makes sense, because the lighter object can't output as much force as a heavier object.
    
4. Two objects are connected by a light string that passes over an ideal pulley. The $200g$ bunch of bananas is initially $140cm$ above the floor. The bananas are released from rest. How long does it take for the $200g$ banana bunch to hit the floor?

    ![image-20201003172346963](macdougall_skyler_week6.assets/image-20201003172346963.png)
    $$
    F_{net_{L}}=m_Lg-m_sg\\
    F_{net_L}=m_La\\
    a_L=\frac{m_Lg-m_sg}{m_L}\\
    x_f=x_i+v_it+\frac12at^2\\
    x_f=x_i+v_it+\frac12\frac{m_Lg-m_sg}{m_L}t^2\\
    x_f=x_i\cancel{+v_it}+\frac12\frac{m_Lg-m_sg}{m_L}t^2\\
    x_f=x_i+\frac12\frac{m_Lg-m_sg}{m_L}t^2\\
    x_f-x_i=\frac12\frac{m_Lg-m_sg}{m_L}t^2\\
    2(x_f-x_i)=\frac{m_Lg-m_sg}{m_L}t^2\\
    \frac{2(x_f-x_i)m_L}{m_Lg-m_sg}=t^2\\
    \sqrt{\frac{2(x_f-x_i)m_L}{m_Lg-m_sg}}=t\\
    \sqrt{\frac{2(0-140cm)(200g)}{(200g)(-9.81m/s^2)-(50g)(-9.81m/s^2)}}=t\\
    t=35.2ms
    $$

# Work and Kinetic Energy

1. The two ropes show n in a birds-eye view of the diagram are used to drag a crate $3.00m$ horizontally across a rough floor. The tensions in the two ropes are $T_1=402N;\ T_2=275N$. The magnitude of the kinzetic friction force is $f_k=500N$. 
    1. What is the direction of the displacement vector $\overrightarrow d$ whose magnitude is $3.00m$? 
        $$
        F_{net}=f_k+\overrightarrow {T_1}+\overrightarrow{T_2}\\
        F_{net}=500N\ang180^\circ+402N\ang20^\circ+275N\ang-30^\circ\\
        F_{net}=115.92342\ang-0.0039061327^\circ\\
    \overline{\underline{|F_{net}\approx0.1kN\ang0^\circ|}}\\
        \overline{\underline{|F_{net}\approx0.1kN\ in\ the\ +x\ direction|}}
        $$
        
    2. How much work is done by the rope $T_1$?
        $$
        W=Fd\cos(\theta)\\
        W=(402N)(3.00m)\cos(20^\circ)\\
        W=1.1332693kJ\\
        \overline{\underline{|W_{T_1}=1.13kJ|}}
        $$
        
    3. How much work is done by the rope $T_2$?
        $$
        W=Fd\cos(\theta)\\
        W=(275N)(3.00m)\cos(-30^\circ)\\
        W=kJ\\
        \overline{\underline{|W_{T_2}=714.J|}}
        $$
        
    4. How much work is done by kinetic friction?
        $$
        W=Fd\cos(\theta)\\
        W=(500N)(3.00m)\cos(180^\circ)\\
        W=-1.5kJ\\
        \overline{\underline{|W_{f_k}=-2kJ|}}
        $$
        
    5. What is the net work done on the crate?
        $$
        \sum W=W_{T_1}+W_{T_2}+W_{f_k}\\
        \sum W=1.13kJ+714J-1.5kJ\\
        \sum W=347.74026J\\
        \overline{\underline{|\sum W=0.3kJ|}}
        $$
        
    
2. Sam's job at the amusement park is to slow down and bring to a stop the boats in a log ride. A boat and its riders have a total mass of $1.2Mg$ and drift with a speed of $1.20m/s$. 
    1. How much total work is done on the boat to slow it from $1.20m/s$ to $0.6m/s$? 
        $$
        W_{total}=\Delta K;\ K=\frac12mv^2\\
        W_{total}=\frac12m(v_f^2-v_i^2)\\
        W_{total}=\frac12(1.2Mg){((0.6m/s)^2-(1.20m/s)^2)}\\
        W_{total}=(0.6Mg){((0.6m/s)^2-(1.20m/s)^2)}\\
        W_{total}=-648J\\
        \overline{\underline{|W_{total}=-0.65kJ|}}
        $$
        
    2. How much total work is done to bring the boat to rest?
        $$
        W_{total}=\Delta K;\ K=\frac12mv^2\\
        W_{total}=\frac12m(v_f^2-v_i^2)\\
        W_{total}=\frac12(1.2Mg){((0m/s)^2-(1.20m/s)^2)}\\
        W_{total}=864\\
        \overline{\underline{|W_{total}=-0.86kJ|}}
        $$
        
    3. What average power is expended in bringing one boat to rest in $8.00s$?
        $$
        P=Fv;\ F=ma;\ a=\frac {\Delta v}t\\
        P=\frac{m(\Delta v)^2}{t}\\
        P=\frac{(1.2Mg){(0-1.20m/s)}^2}{8.00s}\\
        P=216W\\
        \overline{\underline{|P=0.22kW|}}
        $$
        
    4. Instead of having Sam stop each boat by hand, a large spring is placed at the end of the ride such that each boat compresses the spring a distance $x$ from its equilibrium as it comes to a halt. The work $W$ required to stretch a spring from an initial position $x_i$ to a final position $x_f$ is $W=\frac12k(x_f^2-x_i^2)$. Assuming a boat is traveling as in part 2.2, find the spring constant required to stop the boat in no more than $2.00m$. 
        $$
        W=\frac12k(x_f^2-x_i^2)\\
        k=\frac{2W}{x_f^2-x_i^2}\\
        k=\frac{2(-0.86kJ)}{2.00m^2\cancel{-0^2}}\\
        k=432N/m\\
        \overline{\underline{|k=0.43kN/m|}}
        $$
    
3. A catapult launcher on an aircraft carrier accelerates a jet from rest to $72.0m/s$. The work done by the catapult during the launch is $76.0MJ$. During the launch, the system is considered ideal, and all work done on the jet is done by the catapult.
    1. What is the mass of the jet?
        $$
        W=\frac12mv^2\\
        m=\frac{2W}{v^2}\\
        m=\frac{2(76.0MJ)}{(72.0m/s)^2}\\
        m=29.320988Mg\\
        \overline{\underline{|m=29.3Mg|}}
        $$
        
    2. If the jet is in contact with the catapult for $2.00s$, what is the power output of the catapult?
        $$
        P=\frac{m(\Delta v)^2}{t};\ m=\frac{2W}{\Delta v^2}\\
        P=\frac {W}{t}\\
        P=\frac{76.0MJ}{2.00s}\\
        P=38.0MW\\
        \overline{\underline{|P=38.0MW|}}
        $$
        
    
    