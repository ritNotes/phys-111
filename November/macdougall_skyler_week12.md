# Week 12 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 11/13/2020

# Simple Harmonic Motion (Oscillation)

1. How long must a simple pendulum be in order to have a period of simple harmonic motion of $1.00s$ when operated on the Moon?
    (note: $g_{moon}=1.6m/s^2$)
    $$
    T=2\pi\sqrt{\frac{L}{g}}\\
    L=\frac{T^2g}{4\pi^2}\\
    L=\frac{(1.00s)^2(1.6m/s^2)}{4\pi^2}\\
    L=0.04052847346m\\
    \overline{\underline{|L=0.04m|}}
    $$

2. A certain simple pendulum has a period of simple harmonic oscillation of $T_0$ when operated on Earth. If the length of the simple pendulum is tripled, what is the new period of the simple harmonic oscillations, assuming the system is still on earth? Express the answer as a multiple or fraction of $T_0$.
    $$
    T=2\pi\sqrt{\frac{L}{9.81m/s^2}}\\
    T=19.67951414\sqrt L\\
    answer=\frac{T_{3L}}{T_{0}}=\frac{19.67951414\sqrt{3m}}{19.67951414\sqrt{1m}}=\sqrt3\\
    T_{3L}=\sqrt3T_0\\
    \overline{\underline{|T_{3L}=1.73T_0|}}
    $$

3. Vanya is trying to design a metronome that will count our four beats per second. She decides to use a simple pendulum as her system where one beat corresponds to a full cycle of a simple harmonic motion. To build the metronome she attaches a small weight having a mass of $m=390g$ to a long string, of negligible mass. How long must the string be in order for the metronome to maintain cadence that Vanya requires?
    $$
    L=\frac{T^2g}{4\pi^2};\ T=\frac1{f}=\frac1{4Hz}=0.25s;\ g=9.81m/s^2\\
    L=\frac{(0.25s)^2(9.81m/s^2)}{4\pi^2}\\
    L=1.553063768cm\\
    \overline{\underline{|L=1.55cm|}}
    $$

4. To measure the local acceleration due to gravity on planet Astroworld, Travis takes a string having a length of $L=2.40m$ and a small ball bearing having a mass of $m=102g$. When operated as a simple pendulum on planet Astroworld, the system has a period of oscillation of $T=3.53s$. What is the magnitude of the local acceleration due to gravity on planet Astroworld?
    $$
    g=\frac{4\pi^2L}{T^2};\ T=3.53s;\ L=2.40m\\
    g=\frac{(2.40m)4\pi^2}{(3.53s)^2}\\
    g=7.603640367m/s^2\\
    \overline{\underline{|g=7.60m/s^2|}}
    $$

5. The position as a function of time $t$ of a $m=50.0g$ mass attached to a spring is given by
    $$
    x(t)=(2.00cm)\cos\{(10.0rad/s)t\}
    $$
    Given:
    $$
    x=A\cos(\omega t)\\
    \omega=\frac{2\pi}{T}=2\pi f\\
    T=period\\
    A=amplitude
    $$
    

    1. What is the amplitude (maximum displacement) of the motion?
        $2.00m$

    2. What is the period?
        $$
        T=\frac{2\pi}{\omega}\\
        T=\frac{2\pi}{10.0rad/s}\\
        T=0.6283185307s\\
        \overline{\underline{|T=0.628s|}}
        $$

    3. What is the spring constant? 
        ($T_{spring}=2\pi \sqrt{\frac mk}$)
        $$
        T=2\pi\sqrt{\frac{m}{k}}\\
        \frac{T}{2\pi}=\sqrt{\frac{m}{k}}\\
        \frac{T^2}{4\pi^2}=\frac{m}{k}\\
        k=\frac{m4\pi^2}{T^2}\\
        k=\frac{(50.0g)4\pi^2}{(0.628s)^2}\\
        k=5.00N/m\\
        \overline{\underline{|k=5.00N/m|}}
        $$

    4. What is the total mechanical energy? 
        ($E=K_{max}=U_{max};\ U_{max}=\frac12kA^2$)
        $$
        U_{max}=\frac12kA^2\\
        U_{max}=\frac12(5.00N/m)(2.00cm)^2\\
        U_{max}=1.00mJ\\
        \overline{\underline{|U_{max}=1.00mJ|}}
        $$

    5. What is the maximum speed?
        ($K_{max}=\frac12mv_{max}^2$)
        $$
        K=\frac12mv^2;\ K_{max}=U_{max}\\
        v_{max}=\sqrt{\frac{2U}{m}}\\
        v_{max}=\sqrt{\frac{2(1.00mJ)}{50.0g}}\\
        v_{max}=0.2m/s^2\\
        \overline{\underline{|v_{max}=0.2m/s^2|}}
        $$

    6. Determine the position at $t=0.400s$.
        $$
        x(t)=(2.00cm)\cos\{(10.0rad/s)t\}\\
        x(t)=(2.00cm)\cos(4rad)\\
        x=-0.01307287242m\\
        \overline{\underline{|x=-1.31cm|}}
        $$

6. A $m=0.500kg$ mass is attached to a spring and oscillates on a smooth horizontal surface. Ignore friction and air resistance. The velocity-time graph is shown below:
    ![image-20201110144523829](macdougall_skyler_week12.assets/image-20201110144523829.png)

    1. What is the period of motion?

        $T=4.00s$

    2. What is the maximum speed of the mass?
        $v_{max}=2.0m/s$

    3. What is the maximum displacement of the motion?
        $$
        A=\frac{-v_{max}}{\omega};\ \omega=\frac{2\pi}{T}\\
        A=\frac{-v_{max}T}{2\pi}\\
        A=\frac{-(2.0m/s)(4.00s)}{2\pi}\\
        A=-1.273239545m\\
        x_{max}=|A|\\
        \overline{\underline{|x_{max}=1.27m|}}
        $$

    4. What is the spring constant?
        ($T=2\pi\sqrt{\frac{m}{k}}$)
        $$
        k=\frac{m4\pi^2}{T^2}\\
        k=\frac{(0.500kg)4\pi^2}{(4.00s)^2}\\
        k=1.233700550N/m\\
        \overline{\underline{|k=1.23N/m|}}
        $$

    5. What is the maximum elastic potential energy of the mass-spring system?
        ($U_{max}=\frac12kx^2$)
        $$
        U_{max}=\frac12kx^2\\
        U_{max}=\frac12(1.23N/m)(1.27m)^2\\
        U_{max}=1.00J\\
        \overline{\underline{|U_{max}=1.00J|}}
        $$

    6. What is the total energy of the system at time $t=3.50s$?
        $$
        E_{max}=U_{max}=1.00J
        $$
        



# Introduction to Waves

1. A metal guitar has a linear mass density of $\mu=3.20g/m$. What is the speed of transverse waves on this string when its tension is $F_{tension}=90.0N$?
    $$
    v=\sqrt{\frac{F_T}{\mu}}\\
    v=\sqrt{\frac{90.0N}{3.20g/m}}\\
    v=167.7050983m/s\\
    \overline{\underline{|v=168.m/s|}}
    $$
    
2. A transverse wave travels along a stretched horizontal rope. The vertical distance from crest to trough for this wave is $13cm$ and the horizontal distance from crest to trough is $28cm$.

    1. Draw a sketch of one wavelength for this wave in the x-y plane.
        ![image-20201110153110331](macdougall_skyler_week12.assets/image-20201110153110331.png)
    2. What is the amplitude of the wave?
        $7.5cm$
    3. What is the wavelength of the wave?
        $56cm$

