# Week 11 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/6/2020

# Rotational Dynamics

1. A low friction pulley, modeled as a cylinder with a mass $m=0.800kg$ and a radius $r=30.0cm$, has a rope going over it as shown. The tension in the rope is $T_1=12.0N;\ T_2=10.0N$, with $T_1$ on the left and $T_2$ on the right. Both tension forces are tangential. What is the angular acceleration? (Answer to 3 sig figs).
    $$
    I=\frac12mr^2;\ \sum\tau=I\alpha\\
    \tau_1-\tau_2=\frac12mr^2\alpha\\
    r\tau_1-r\tau_2=\frac12mr^2\alpha\\
    T_1-T_2=\frac12mr\alpha\\
    \alpha=\frac{2(T_1-T_2)}{mr}\\
    \alpha=\frac{2(12.0N-10.0N)}{(0.800kg)(30.0cm)}\\
    \alpha=16.\overline6rad/s^2\\
    \overline{\underline{|\alpha=16.7rad/s^2|}}
    $$

2. The $m=2.00kg$ solid disk is spinning about its central axis at $300rpm=10\pi rad/s$. The radius of the disk is $r=30.0cm$. How much friction force mus the brake apply to the rim to bring the disk to rest in $t=3.00s$. The frictional braking force acts tangentially on the circumference of the wheel. 
    $$
    F=\frac{I\alpha}{r};\ I=\frac12mr^2;\ \alpha=\frac \omega t\\\\
    F=\frac{\frac12mr^2*\frac{\omega}{t}}{r}\\
    F=\frac{mr^2\omega}{2rt}\\
    F=\frac{mr\omega}{2t}\\
    F=\frac{(2.00kg)(0.300m)10\pi rad/s}{2(3.00s)}\\
    F=\pi N\\
    \overline{\underline{|F=\pi N|}}
    $$
    

# Angular Momentum

1.  Two flat disks are rotating about a common axis. The top disk is rotating clockwise. The bottom disk is rotating counterclockwise. The top disk is dropped on the bottom disk. After a moment of sliding, the two disks stick together and rotate with the same velocity. All values are shown below:
    $$
    \omega_{1i}=-7.20rad/s\\
    \omega_{2i}=9.80rad/s\\
    I_{1}=1.44\times10^{-4}kgm^2\\
    I_2=3.55\times10^{-4}kgm^2\\
    h=1.00cm
    $$
    

    1. What is the common angular velocity?
        $$
        L_i=L_f;\ L=I\omega\\
        I_1\omega_{1i}+I_2\omega_{2i}=I_{1+2}\omega_f\\
        \omega_f=\frac{I_1\omega_{1i}+I_2\omega_{2i}}{I_{1+2}}\\
        \omega_f=\frac{(1.44\times10^{-4}kgm^2)(-7.20rad/s)+(3.55\times10^{-4}kgm^2)(9.80rad/s)}{(3.55+1.44)\times10^{-4}kgm^2}\\
        \omega_f=4.894188377rad/s\\
        \overline{\underline{|\omega_f=4.89rad/s|}}
        $$
    
2. What is the total kinetic energy before the collision?
        $$
        K=\frac12I\omega^2\\
        K_i=\frac12I_1\omega_{1i}^2+\frac12I_2\omega_{2i}^2\\
        K_i=\frac12(1.44\times10^{-4}kgm^2)(-7.20rad/s)^2+\frac12(3.55\times10^{-4}kgm^2)(9.80rad/s)^2\\
        K_i=20.77958mJ\\
        \overline{\underline{|K_i=20.8mJ|}}
        $$
        
    
3. ... after the collision?
    
2. A figure skater is spinnning at a rate of $10.0rad/s$ with her arms horizontally extended and her moment of inertia of $2.50kgm^2$. 

    1. When she pulls her arms in close to her body, will her angular speed change?
    2. What is her angular speed after she pulls her arms in and reduces her moment of inertia to $1.60kgm^2$?
    3. Calculate her kinetic energy before and after she pulls her arms in. Is kinetic energy conserved? If kinetic energy was lost, where did it go? If it was gained, where did it come from?