# Week 13 Activities Problems

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 11/20/2020

# Beats

1. The little brown bat is a common bas species in North America. It emits echolocation pulses at a frequency of $40kHz$, well above the range of human hearing. To allow observers to "hear" these bats, an electronic bat detector combines the bat's sound wave with a wave created by a tunable oscillator. The resulting beat frequency is isolated, amplified, then played through a speaker. What frequency should the oscillator be set to produce an audible beat frequency of $3.0kHz$?
    $$
    f_{beat}=|f_1-f_2|\\
    f_1=f_{beat}\pm f_2\\
    f_1=3.0kHz\pm40kHz\\
    \overline{\underline{|f_1=43kHz,\ 37kHz|}}
    $$
    

# Sound Waves Interference

1. Two identical sources emit sound of frequency $2.00kHz$ and a speed of $v=343m/s$. They are separated by $d_{diff}=25.7cm$. Assume no loss in intensity with distance.
    1. Calculate the interference heard at a distance of $5.00m$ away along the axis joining the speakers.
        $$
        d_{diff}=n\lambda\\
        \lambda=\frac{d_{diff}}{n}\\
        f=\frac{vn}{d_{diff}}
        $$
        If $2n$ is an even integer, than it is constructive interference. If $2n$ is an odd integer, than it is destructive interference. If $2n$ is not an integer, it is partial interference.
        $$
        f=\frac{vn}{d_{diff}}\\
        n=\frac{fd_{diff}}{v}\\
        n=\frac{(2.00kHz)(25.7cm)}{343m/s}\\
        n=1.498542274\approx1.5\\
        2n=3
        $$
        3 is odd, therefore, it is completely destructive interference.
    
    2. You now increase the frequency of both speakers. Calculate the next frequency for which you will hear destructive interference. 
        $$
        2n=5;\ n=2.5\\
        f=\frac{vn}{d_{diff}}\\
        f=\frac{(343m/s)(2.5)}{25.7cm}\\
        f=3.336575875kHz\\
        \overline{\underline{|f=3.34kHz|}}
        $$
    
    3. Now return to $2.00kHz$. What can you do to get the opposite type of interference as in part 1.1?
        Change the distance, either from the listener to the speakers, or between the speakers.
    
2. 2 isotropic point sources produce sound waves that are in phase with each other at the positions of the sources. Source 1 is $d_1=17.1m$ from the observer, and Source 2 is $d_2=41.6m$ from the observer. What are the 3 lowest frequencies in the audible range at which fully destructive interference will occur at the position of the observer? Ignore the loss of intensity. Use $v=343m/s$ for the speed of sound in air.
    Waves are in complete destructive interference when they are one half wavelength out of phase. This means that the difference between $d_1$ and $d_2$ has to be equal to some half-integer multiple of the wavelength. Therefore:
    $$
    d_{diff}=d_2-d_1=41.6m-17.1m=24.5m\\
    d_{diff}=\frac n2\lambda\\
    \lambda=\frac{2d_{diff}}{n}
    $$
    Given $v$, we can choose $n$ such that it fits within the human hearing range $20Hz-20kHz$.
    $$
    f=\frac v\lambda\\
    f=\frac{vn}{2d_{diff}}\\
    20Hz\le f\le20kHz
    $$
    Inputing $f=20Hz$ does not give us an integer $n$, but rather gives us guidance as to where our first value will be. The first integer above $n_{20Hz}=2.857...$ is 3. Thus, the 3 lowest frequencies where destructive interference will be calculated using $f=\frac{vn}{2d_{diff}}$, where $n=3,4,5$.
    $$
    \overline{\underline{|f=21.0Hz,\ 35.0Hz,\ 49.0Hz|}}
    $$
    

# Standing Waves

1. A string of length $L=2.00m$ is fixed at both ends and tightened until the wavespeed is $40.0m/s$.
    1. What is the wavelength of the standing wave, given that the wave has 6 antinodes?
        $$
        \lambda=\frac{2L}{n}\\
        \lambda=\frac{2(2.00m)}{6}\\
        \lambda=0.\overline6m\\
        \overline{\underline{|\lambda=0.67m|}}
        $$
    
    2. What is the frequency of the standing wave stated above?
        $$
        f=\frac v\lambda\\
        f=\frac{40.0m/s}{0.67m}\\
        \overline{\underline{|f=60Hz|}}
        $$
    
    3. What is the fundamental frequency of the stretched string?
        $f_0=10Hz$
    
    4. If tension in the string is $T=3.60N$, what is the mass per unit length of the string?
        $$
        v=\sqrt{\frac{T}{\mu}}\\
        \mu=\frac{T}{v^2}\\
        \mu=\frac{3.60N}{(40m/s)^2}\\
        \overline{\underline{|\mu=2.25\times10^{-3}kg/m|}}
        $$
        
2. A $m=12.5g$ clothesline is stretched with a tension $T=22.1N$ between 2 poles $L=7.66m$ apart.
    1. What is the fundamental frequency?
        $$
        f=\frac{v}{\lambda};\ v=\sqrt{\frac{T}{\mu}};\ \lambda=2L;\ \mu=\frac mL\\
        f=\frac{\sqrt{\frac{T}{\frac{m}L{}}}}{2L}\\
        f=\frac{\sqrt{\frac{22.1N}{\frac{12.5g}{7.66m}}}}{2(7.66m)}\\
        f=7.596206281Hz\\
        \overline{\underline{|f=7.60Hz|}}
        $$
    
    2. What is the frequency of the second harmonic?
        $$
        f_n=nf_0\\
        f_n=2(7.60Hz)\\
        \overline{\underline{|f_n=15.20Hz|}}
        $$
        
    
    3. Make a simple sketch of the standing wave pattern corresponding to the second harmonic.
        ![image-20201117162410368](macdougall_skyler_week13.assets/image-20201117162410368.png)
    
    4. If the tension in the clothesline is increased, does the frequency of the second harmonic change? 
    
        The frequency will also increase. This is because $f=\sqrt{\frac{T}{4mL}};\ f\propto T^{\frac12}$.
    
    5. If a heavier rope is used, but is stretched the same distance under the same tension, does the frequency of the second harmonic change?
        The frequency will decrease. This is because $f=\sqrt{\frac{T}{4mL}};\ f\propto \frac1{m^{\frac12}}$.

